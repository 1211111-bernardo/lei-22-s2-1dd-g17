import org.junit.Test;

import java.time.LocalDate;
import java.time.Period;

public class AgeCalculator_Test {


    @Test
    public void testCalculateAge_Success() {
        /**
         * Birthday Date of the user invented for the test
         */
        LocalDate birthdate = LocalDate.of(1961, 5, 17);
        /**
         * Today Date invented for the test
         */
        LocalDate today = LocalDate.of(2016, 7, 12);
        /**
         * Peridod P is the period between the two dates
         */
        Period p = Period.between(birthdate, today);
        /**
         * For the method to be right the period must be 55 years
         */
        if (p.getYears()==55){
            System.out.println("Teste Correto");
        }
        /**
         * If not that means that the Age Calculator method is wrong
         */
        else{
            System.out.println("Teste Incorreto");
        }
    }
}