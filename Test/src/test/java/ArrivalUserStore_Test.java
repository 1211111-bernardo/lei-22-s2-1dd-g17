package src.test.java;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ArrivalUserStore_Test {
// Java program for the above approach

    public ArrivalUserStore_Test (){

    }
        /**
        * Function to print difference in time start_date and end_date
        */
       void findDifference(String start_date, String end_date)
        {
            /** SimpleDateFormat converts the string format to date object
             */
            SimpleDateFormat sdf = new SimpleDateFormat(
                    "dd-MM-yyyy HH:mm:ss");

            // Try Block
            try {

                // parse method is used to parse the text from a string to produce the date
                Date d1 = sdf.parse(start_date);
                Date d2 = sdf.parse(end_date);

                // Calculate time difference
                long difference_In_Time
                        = d2.getTime() - d1.getTime();

                // Calculate time difference in hours

                long difference_In_Hours
                        = (difference_In_Time
                        / (1000 * 60 * 60))
                        % 24;
                // Calculate time difference in days
                long difference_In_Days
                        = (difference_In_Time
                        / (1000 * 60 * 60 * 24))
                        % 365;

                System.out.printf("Difference between two dates is: %d days and %d hours ",difference_In_Days,difference_In_Hours);
            }
            // Catch the Exception
            catch (ParseException e) {e.printStackTrace();
                System.out.println("Ocorreu um erro");
            }
        }

        /**
         * Prints all data to validate test
         * @param args
         */
        public void DiferenceTime_Test(String[] args)
        {
            // Given start Date
            String start_date
                    = "10-01-2018 01:10:20";

            // Given end Date
            String end_date
                    = "10-06-2020 06:30:50";

            // Function Call
            findDifference(start_date, end_date);
        }



}
