package src.test.java;

public class Main_test {
    public static void main (String[] args) {
        ArrivalUserStore_Test TimeDifference = new ArrivalUserStore_Test();
        TimeDifference.findDifference("20-05-2022 12:00:00","21-05-2022 13:00:00");
        PerformanceStore_Test PerformanceTest = new PerformanceStore_Test();
        int[] List  = {1,2,3,4,5,6,7,8,9,10};
        System.out.println("\nCorrect Max Sum = 55");
        System.out.printf("Calculated Max Sum = %d \n",PerformanceTest.CalculateMaxSum(List));
        if (PerformanceTest.CompareDate("30/5/2022","30/5/2022")){
            System.out.println("The dates are equal");
        }else
            System.out.println("The dates are different");
    }
}
