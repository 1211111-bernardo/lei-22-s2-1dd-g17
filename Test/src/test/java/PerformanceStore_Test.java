package src.test.java;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PerformanceStore_Test {
    public PerformanceStore_Test() {

    }

    /**
     * This method receives a list containing integers
     * @param nums - int[] list
     * @return maximum sum of the integers list
     */
    public int CalculateMaxSum(int[] nums) {

        int n = nums.length;
        int maximumSubArraySum = Integer.MIN_VALUE;
        int start = 0;
        int end = 0;

        for (int left = 0; left < n; left++) {

            int runningWindowSum = 0;

            for (int right = left; right < n; right++) {
                runningWindowSum += nums[right];

                if (runningWindowSum > maximumSubArraySum) {
                    maximumSubArraySum = runningWindowSum;
                    start = left;
                    end = right;
                }
            }
        }
        return maximumSubArraySum;
    }

    /**
     * This method compares to date to check if they are equal or not
     * @param Date1
     * @param Date2
     * @return true or false depending on the given dates
     */
    public boolean CompareDate (String Date1,String Date2)  {
        if (Date1.equals(Date2)){
            return true;
        } else
            return false;
    }
}
