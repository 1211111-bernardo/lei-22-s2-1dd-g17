# Supplementary Specification (FURPS+)

## Functionality

_Specifies functionalities that:_

- _are common across several US/UC;_
- _are not related to US/UC, namely: Audit, Reporting and Security._

| **Function** | **Description/Example** |                                       
|:------------------------|:--------------------------------------------|
| **Authorization** | Support for managing the status of the SMS (informative SMS needs to be approved to be send). |
| **Printing** | Facilities related with printing events (e.g.: vaccine type/name/brand) in the system. |
| **Email** | Ability to notify the user via email/SMS |
| **Requesting** | Support for generating Digital Certificates |
| **Analysis** | Support for showing the efficiency in responding of the vaccination  |
| **Benchmarking** | Support for test the implemented alghoritm in worst-case scenarios |
| **Help** | Existence of informative support for users of the system |
| **Security** | Controlled access to certain system features (e.g.: vaccine data) |





## Usability 

_Evaluates the user interface. It has several subcategories,
among them: error prevention; interface aesthetics and design; help and
documentation; consistency and standards._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| **Operability** | " "The user should introduce his/her SNS user number, select the vaccination center..." |
| **Communicativeness** | "The application must support, at least, the Portuguese and the English languages." |
| **Consistency** ||
| **Aesthetics** ||
| **Documentation** | "The user manual must bedelivered with the application." |
| **Human Factores** |"...a nurse responsible for administering the vaccine will use the application to check thelist of SNS users..."|

(fill in here )

## Reliability
_Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| **Acuracy** |  |
| **Frenquency of failure** |  |
| **Predictability** | |

(fill in here )

## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| **Efficiency** | "Adopt best practices for identifying requirements..." | 
| **Resource Consumption** | "The application should use object serialization..." |
| **Response Time** | "...observation of the execution time of the algorithms for inputs of variable size..." |

(fill in here )

## Supportability
_The supportability requirements gathers several characteristics, such as:
testability, adaptability, maintainability, compatibility,
configurability, installability, scalability and more._ 

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| **Testability** | "...implement unit tests for all methods..." |
| **Configurability** | | 
| **Adaptability** | "The application must support, at least, the Portuguese and the English languages."|
| **Maintainability** | "... adopt recognized coding standards..." |


(fill in here )


## +

### Design Constraints

_Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc._
  
| **Example Requirements** | **Project** |                                       
|:----------------------------|:---------------------------------------------------|
| **Programming Languages** | "The application must be developed in Java language..." |
| **Mandatory Standards** | "Adopt recognized coding standards..." | 
| **Development Tools** | "IntelliJ IDE or NetBeans." |
| **Software process** | Scrum - "...detailed requirements of each sprint..." |

(fill in here )


### Implementation Constraints

_Specifies or constraints the code or construction of a system such
such as: mandatory standards/patterns, implementation languages,
database integrity, resource limits, operating system._

| **Example Requirements** | **Project** |                                       
|:----------------------------|:----------------------------------------------------|
| **Implementation Languages** | "The application must be developed in Java language..." |
| **Database Integrity** | "Adopt recognized coding standards for identifying requirements..." | 
| **Resource Limits** | |
| **Operating System** | |

(fill in here )


### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| ... | ... |



(fill in here )

### Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| ... | ... |

(fill in here )
