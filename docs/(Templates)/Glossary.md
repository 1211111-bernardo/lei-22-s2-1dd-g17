# Glossary

**Terms, Expressions and Acronyms (TEA) must be organized alphabetically.**

(To complete according the provided example)

| **_TEA_** (EN)  | **_TEA_** (PT) | **_Description_** (EN)                                           |                                       
|:------------------------|:-----------------|:--------------------------------------------|
| **Administrator** |	**Administrador**	| A person responsible for carrying out the administration of a business or organization.
| **Adverse Reactions** |	**Reações Adversas**	| Unexpected effect suspected to be caused by a medicine.
| **AGES** | **AGES** | Acronym for "Agrupamento de Centros de Saúde. |
| **ARS**| **ARS** | Acronym for "Administração Regional de Saúde. |
| **ASTRA ZENECA** |	**ASTRA ZENECA**	| Type of vaccine for Covid-19.
| **Asymptotic Behavior** |	**Comportamento Assintótico**	| The behavior of infinitely long sequences of random variables.
| **Benchmark Algorithm** | **Algoritmo de Benchmark** | Algorithm used for analysis and comparison of implemented algorithms, in order to assess its performance. |
| **Brute-Force Algorithm** | **Algoritmo por Busca Exaustiva** | Straightfoward methods of solvinga problem that rely on sheer computing power and trying every possibility rather than advanced techniques to improve efficiency. |
| **Coding Standards** | **Padrões do Código** | Collections of rules and guidelines that determine the programming style,procedures and methods for a programming language. |
| **Clerk** | **Administrativo** | Person responsible for carrying out various business supporting activities on the system. |
| **CLK** | **ADM** | Acronym for _Clerk_.|
| **Computational Complexity Analysis** | **Análise de Complexidade Computacional** | Analysis of the amount of resources required to run the algorithm. |
| **Covid-19**| **Covid-19** | Infectious disease caused by Sars-Cov-2 Virus. |
| **Dengue** | **Dengue** | A viral infection that is transmitted by mosquitoes. |
| **DGS** |	**DGS**	| Acronym for "Direção Geral de Saúde".
| **EU Covid Digital Certificate** | **Certificado Digital De Covid da EU** | Digital proof that a person has either, been vaccinated against COVID-19, received a negative test result or recovered from COVID-19. |
| **ESOFT** |	**ESOFT** |	Acronym for "Engenharia de Software".
| **Fifo Queue** |	**Fifo Queue**	| Queue that operates on a first-in, first-out (FIFO) principle. This means that the request (like a customer in a store or a print job sent to a printer) is processed in the order in which it arrives.
| **Healthcare Center** | **Centro de Saúde** | A center responsible to provide and assist healthcare services to SNS users. |
| **Immunization** | **Imunidade** | A proccess where a person becomes protected against some type of disease through vaccination. |
| **Intellij Idea** | **Intellij Idea** | Integrated Development Environment (IDE) for JVM languages designed to maximize developer productivity. |
| **Jacoco Plugin** | **Plugin Jacoco** | Runs the coverage by instrumenting Java code through a runtime agent. |
| **Java** | **Java** | Java is a programming language and computing platform. |
| **JavaDoc** | **JavaDoc** | Document generator tool in Java programming language for generating standard documentation in "HTML format". |
| **JUnit5** | **JUnit5** | Is the next generation of JUnit.The goal is to create an up-to-date foundation for developer-side testing on the "JVN". |
| **LAPR2** |	**LAPR2** |	Acronym for "Laboratório /Projeto II".
| **LEI** | **LEI** |	Acronym for "Licenciatura em Engenharia informática".
| **Lot Number** |	**Número de lote**	|  Identification number assigned to a particular quantity or lot of material.
| **MATCP** |	**MATCP** |	Acronym for "Matemática Computacional".
| **MATDISC** |	**MATDISC** |	Acronym for "Matemática Discreta".
| **MODERNA** |	**MODERNA**	| Type of vaccine for Covid-19.
| **Nurse**| **Enfermeiro/a** | Caregiver for patients that seeks to help to manage physical needs and treat health conditions. |
| **OO Software** | **Software de Orientação a Objetos** | Is a software design technique that is used in objected-oriented programming. |
| **Pandemic** |	**Pandémico**	| An outbreak of a disease that occurs over a wide geographic area and typically affects a significant proportion of the population.
| **PBL (Project Based Learning)**	| **PBL (Project Based Learning)** |	Acronym for "Project Based Learning".
| **Pfizer** | **Pfizer** | A type of Covid-19 vaccine that contains a small and harmless piece of Covid-19's mRNA (messenger RNA). | 
| **PPROG** |	**PPROG** |	Acronym for "Paradigmas da Programação".
| **Receptionist** | **Rececionista** | Someone that has the responsibility to perform multiple administrative tasks. |
| **Recovery Period** |	**Tempo de Recuperação**	| Period of time that takes for a person to recover.
| **RUC** |	**RUC** |	Lapr2 Course´s coordinator.
| **Schedule Appointment** |	**Agendamento de consultas**	| An arrangement to meet a person or be at a place at a certain time  
| **Smallpox** | **Variola** | Very contagious and deadly disease that afected humans and was already wiped out. |
| **SNS**| **SNS**| Acronym for "Serviço Nacional de Saúde". A Service provided by the Portuguese Government, where everyone has the right to health care. |
| **SNS User** |	**Utilizador da SNS**	| A person who uses SNS
| **SVG Format** | **Formato SVG** | Scalable Vector Graphic (SVG) is a unique type of image format. Unlike other varieties, SVGs don´t rely on unique pixels to make up the imagesyou see.Instead, they use "vector" data. |
| **Tetanus** | **Tetano** | Infection caused by a type of bacteria called "Clostridium bacteria". |
| **UC** |	**UC** |	Acronym for "Unidade Curricular".
| **User's Health Data** |	**Dados de Saúde do Utilizador**	| Contains all data about the health of the user.
| **User Manual** |	**Manual Do Usuário**	| Document intended to give assistance to people on how to use a product.
| **Vaccines** |	**Vacinas** |	A preparation that is administered to stimulate the body's immune response against a specific infectious agent or disease.
| **Vaccination Center**| **Centro de Vacinação** | A center where is administered vaccines to the SNS users. |

