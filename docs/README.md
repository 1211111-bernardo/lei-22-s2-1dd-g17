# Integrating Project for the 2nd Semester of LEI-ISEP 2021-22 

# 1. Team Members

The teams consists of students identified in the following table. 

| Student Number	| Name |
|--------------|----------------------------|
| **1211111**  | Bernardo Azevedo           |
| **1211115**  | Gabriel Sousa             |
| **1211119**  | Marco Moreira              |
| **1201440**  | Daniel Coutinho            |
| **1210820**  | Jorge Sousa                |
| **1211024**  | Nuno Tavares               |



# 2. Task Distribution ###


Throughout the project's development period, the distribution of _tasks / requirements / features_ by the team members was carried out as described in the following table. 

**Keep this table must always up-to-date.**

| Task                      | [Sprint A](SprintA/README.md) | [Sprint B](SprintB/README.md) | [Sprint C](SprintC/README.md) |  [Sprint D](SprintD/README.md) |
|-----------------------------|------------|------------|------------|------------|
| Glossary  |  [all](SprintA/Glossary.md)   |   [all](SprintB/Glossary.md)  |   [all](SprintC/Glossary.md)  | [all](SprintD/Glossary.md)  |
| Use Case Diagram (UCD)  |  [all](SprintA/UCD.md)   |   [all](SprintB/UCD.md)  |   [all](SprintC/UCD.md)  | [all](SprintD/UCD.md)  |
| Supplementary Specification   |  [all](SprintA/FURPS.md)   |   [all](SprintB/FURPS.md)  |   [all](SprintC/FURPS.md)  | [all](SprintD/FURPS.md)  |
| Domain Model  |  [all](SprintA/DM.md)   |   [all](SprintB/DM.md)  |   [all](SprintC/DM.md)  | [all](SprintD/DM.md)  |
| US 03 (SDP Activities)  |     |  [1211119](SprintB/DM.md)  |   |  |
| US 09 (SDP Activities)  |     |  [1211024](SprintB/DM.md)  |   |  |
| US 10 (SDP Activities)  |     |  [1211111](SprintB/DM.md)  |   |  |
| US 11 (SDP Activities)  |     |  [1210820](SprintB/DM.md)  |   |  |
| US 12 (SDP Activities)  |     |  [1211115](SprintB/DM.md)  |   |  |
| US 13 (SDP Activities)  |     |  [1201440](SprintB/DM.md)  |   |  |
| ...  |  ...   | ...   | ...  | ... |
|

