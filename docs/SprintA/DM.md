# OO Analysis #

The construction process of the domain model is based on the client specifications, especially the nouns (for _concepts_) and verbs (for _relations_) used. 

## Rationale to identify domain conceptual classes ##
To identify domain conceptual classes, start by making a list of candidate conceptual classes inspired by the list of categories suggested in the book "Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development". 


### _Conceptual Class Category List_ ###

**Business Transactions**

*

---

**Transaction Line Itemss**

*

---

**Product/Service related to a Transaction or Transaction Line Item**

*  
Vaccine, 
SNS

---


**Transaction Records**

*  

---  


**Roles of People or Organizations**

* 
SNS Users, 
Receptionists, 
Nurses, 
Administrators, 
Coordinators

---


**Places**

*  
Community Mass Vaccination Center, 
HealthCare Center

---

** Noteworthy Events**

* 
 Adverse Reactions, 
 SNSUsers List, 
 Vaccination Centers capacity, 
 Core Information, 
 Vaccination Process, 
 Vaccination Schedule, 
 Vaccination Administration, 
 Vaccination Certificate, 
 Administration Process, 
 
---


**Physical Objects**

*

---


**Descriptions of Things**

*  
Vaccine Typ, 
Vaccination Process, 
Vaccination Brand, 

---


**Catalogs**

*  

---


**Containers**

*  

---


**Elements of Containers**

*  

---


**Organizations**

*  
DGS(Company), 
WHO, 
ARS, 
AGES, 

---

**Other External/Collaborating Systems**

*  


---


**Records of finance, work, contracts, legal matters**

* 

---


**Financial Instruments**

*  

---


**Documents mentioned/used to perform some work/**

* 
---







| Concept (A) 		|  Association   	|  Concept (B) |
|----------	   		|:-------------:		|------:       |
| Company  	| manages    		 	| Community Mass Vaccination Center
| | |HealthCare Center
| | aplies| Vaccine
| | responsible for | Vaccination Process
| | |
| HealhCare Center| associated with | ARS and AGES
| | can administer | Vaccine Type
| | has | Coordinator
| | | Nurse
| | | Administrator
| | | Receptionists
| | |
| Community Mass Vaccination Center | can administer one single | Vaccine Type
| | has | Coordinator
| | | Nurse
| | | Administrator
| | | Receptionists
| | |
| Vaccine | is of | Vaccine Type
| | |
| Vaccine Type | is of | Vaccine Brand
| | created by | Administrator
| | has a | Vaccination Process
| | |
| Vaccine Schedule | created by/for | SNS User
| | for taking | Vaccine Type
| | checked by | HealthCare Center
| | | Community Mass Vaccination Center
| | |
| Vaccination Process | managed by | Coordinator
| | | DGS
| | employs registered | Nurses
| | | Receptionists
| | |
| Administrator | can create | Vaccine Type
| | registers the enrolled | HealtCare Center
| | | Community Mass Vaccination Center
| | | SNS Users
| | | Coordinator
| | | Receptionists
| | | Nurses
| | manages | Core Information
| | | 
| Nurse | Records | Adverse Reactions
| | Works on | Vaccination Process
| | | HealthCare Center
| | | Community Mass Vaccination Center
| | cheks | SNS Users List
| | administers | Vaccine Type
| | can issue | Vaccination Certificate
| | |
| Coordinator | manages | Vaccination Process
| | works on | HealthCare Center
| | | Community Mass Vaccination Center
| | |
| SNS User | creates | Vaccine Schedule
| | receives a | Vaccination Type
| | asks for | Vaccination Certificate
| | |
| Receptionist | works on | Vaccination Process
| | helps the ... to schedule the vaccine | SNS User
| | checks | SNS Users List
| ...  	| ...    		 	| ...  |



## Domain Model

**Do NOT forget to identify concepts atributes too.**

**Insert below the Domain Model Diagram in a SVG format**

![DM.svg](DM.svg)



