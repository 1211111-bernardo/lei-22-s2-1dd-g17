# US 009 - To register a vaccination center to respond to a certain pandemic

## 1. Requirements Engineering


### 1.1. User Story Description

As an administrator, I want to register a vaccination center to respond to a certain pandemic.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> "The DGS has Administrators who administer the application. Any Administrator uses the application to register centers, SNS users, center coordinators, receptionists, and nurses enrolled in the vaccination process."

**From the client specifications:**

>

### 1.3. Acceptance Criteria

> No acceptance criteria specified.

### 1.4. Found out Dependencies

* No dependency was found.

### 1.5 Input and Output Data

**Input data**
    
(new center information)

* name
* adress
* phone number
* e-mail adress
* fax number
* website adress
* opening and closing hours
* slot duration
* maximum number of vaccines given per slot
* set a coordinator
* specify a vaccine type (in case of Community Mass Vaccination Center)
    
**Output data**

* in(success) of the operation

### 1.6. System Sequence Diagram (SSD)

![US_09-SSD](US_09-SSD.svg)


### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.* 


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.* 

![US_09-MD](US_09-MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).* 



## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1 : Starts the registration of a new vaccination center | instantiating a new vaccination center?							 | Company            | Creator                             |
|		 |        interacting with the actor?  | RegisterVaccCenterUI | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model|
| Step 2 : Requests to fill vaccination center data (e.g. name, adress, phone number, slot duration) |n/a					 |             |                              
| Step 3 : Types requested data| saving the input data?			 | VaccCenter           | IE: Data will be saved as object attributes                           |
| Step 4 : Asks to set a coordinator |		n/a|             |                              |              
| Step 5 :	Set a coordinator |	saving the chosen coordinator?						 | VaccCenter             | IE: Data will be saved as object attribute                             |
| Step 6 : Shows all data and requests confirmation |		n/a					 |             |                              |
| Step 7 : Confirms data | validating the data?							 | Company            |     IE:Knows all authenticaded data                         |
		 | | saving the created vaccination center |Organization |IE: owns all its users. |
| Step 8 : Checks all data and informs the operation success		 |		informing operation success					 |RegisterVaccCenterUI|           IE: is responsible for user interactions                   |  


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * VaccCenter

Other software classes (i.e. Pure Fabrication) identified: 

 * RegisterVaccCenterUI  
 * RegisterVaccCenterController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![US_09-SD](US_09-SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US_09-CD](US_09-CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**AddUser_Test 1:** Check that it is not possible to create an instance of the Example class with null values. 

	@AddUser_Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.* 

# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





