# Supplementary Specification (FURPS+)

## Functionality

_Specifies functionalities that:_

- _are common across several US/UC;_
- _are not related to US/UC, namely: Audit, Reporting and Security._

| **Function** | **Description/Example** |                                       
|:------------------------|:--------------------------------------------|
| **Auditing** | See statistics and charts of the vaccination process and data from other centers in order to evaluate the vaccination process. |
| **Reporting** | Obtain statistics and charts of the vaccination process, generate reports and acquire data from other centers, including data from law systems. |
| **Authorization** | Support for managing the status of the SMS (informative SMS needs to be approved to be send). |
| **Printing** | Facilities related with printing events (e.g.: vaccine type/name/brand) in the system. |
| **Email** | Ability to notify the user via email/SMS |
| **Requesting** | Support for generating Digital Certificates |
| **Analysis** | Support for showing the efficiency in responding of the vaccination  |
| **Benchmarking** | Support for test the implemented alghoritm in worst-case scenarios |
| **Help** | Existence of informative support for users of the system |
| **Security** | Controlled access to certain system features (e.g.: vaccine data) |





## Usability 

_Evaluates the user interface. It has several subcategories,
among them: error prevention; interface aesthetics and design; help and
documentation; consistency and standards._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| **Operability** | " "The user should introduce his/her SNS user number, select the vaccination center..." |
| **Communicativeness** | "The application must support, at least, the Portuguese and the English languages." |
| **Consistency** |The interfaces should be developed to make sure elements in a user interface are uniform.|
| **Aesthetics** |The application must be developed in such a way to be appealing, easy to use and, above all, intuitive for each user.|
| **Documentation** | "The user manual must bedelivered with the application." |
| **Human Factores** |"...a nurse responsible for administering the vaccine will use the application to check thelist of SNS users..."|
| **Help** |A user manual must be delivered with the application.|

(fill in here )

## Reliability
_Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| **Accuracy** |For any time interval on one day, the difference between the number of new clients arrival and the number of clients leaving the center every five-minute period is computed.  |
| **Frenquency of failure** |  |
| **Predictability** | |
| **Disaster recovery possibility** | |

(fill in here )

## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| **Efficiency** | "Adopt best practices for identifying requirements..." | 
| **Resource Consumption** | |
| **Response Time** | "...observation of the execution time of the algorithms for inputs of variable size..." |
| **System start-up time** | |
| **System recovery time** | |
| **System setup time** | |
| **System shutdown time** | |
| **Application availability** | |

(fill in here )

## Supportability
_The supportability requirements gathers several characteristics, such as:
testability, adaptability, maintainability, compatibility,
configurability, installability, scalability and more._ 

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| **Testability** | "...implement unit tests for all methods..." |
| **Configurability** | | 
| **Compatibility** | | 
| **Installability** | The app must be able to be installed in any device that has the capability to run JAVA source applications.| 
| **Scalability** | The application should be designed to easily support managing other future pandemic events requiring a massive vaccination of the population. | 
| **Adaptability** | "The application must support, at least, the Portuguese and the English languages."|
| **Maintainability** | Have a primarily modular code structure to allow code reuse. |


(fill in here )


## +

### Design Constraints

_Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc._
  
| **Example Requirements** | **Project** |                                       
|:----------------------------|:---------------------------------------------------|
| **Programming Languages** | "The application must be developed in Java language..." |
| **Mandatory Standards** | "Adopt recognized coding standards..." | 
| **Development Tools** | "IntelliJ IDE or NetBeans." |
|  | Develop the application graphical interface in JavaFX 11.|
|  | Adopt OO software design.|
|  | The JaCoCo plugin should be used to generate the coverage report.|
|  | Javadoc to generate useful documentation for Java code.|
|  | Record all the images/figures produced during the software development process in SVG format.|
| **Software process** | Scrum - "...detailed requirements of each sprint..." |



### Implementation Constraints

_Specifies or constraints the code or construction of a system such
such as: mandatory standards/patterns, implementation languages,
database integrity, resource limits, operating system._

| **Example Requirements** | **Project** |                                       
|:----------------------------|:----------------------------------------------------|
| **Implementation Languages** | "The application must be developed in Java language..." |
| **Database Integrity** | "Adopt recognized coding standards for identifying requirements..." | 
| **Resource Limits** | |
| **Operating System** |Implement unit tests for all methods, except for methods that implement Input/Output operations. |



### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| ... | ... |



(fill in here )

### Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._

| **Example Requirements** | **Project** |                                       
|:------------------------|:--------------------------------------------|
| ... | ... |

(fill in here )
