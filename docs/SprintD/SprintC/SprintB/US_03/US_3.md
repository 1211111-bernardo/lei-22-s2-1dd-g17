# US 03 - Register a SNS User

## 1. Requirements Engineering

*In this section, it is suggested to capture the requirement description and specifications as provided by the client as well as any further clarification on it. It is also suggested to capture the requirements acceptance criteria and existing dependencies to other requirements. At last, identify the involved input and output data and depicted an Actor-System interaction in order to fulfill the requirement.*


### 1.1. User Story Description

As an administrator, I want to register a SNS User.

### 1.2. Customer Specifications and Clarifications 

**From the client clarifications**

> **Question:** "In terms of the atributes that you have pointed out i would like to know the limit and rules of those atributes (EX: Phonenumber, birthdate,etc)."

> **Answer:** During Sprint B I will not introduce attribute rules/formats other than the ones that I already introduced (in this forum or in the project description). Please study the concepts and define appropriate formats for the attributes.



> **Question:** "[1] you have already specified that the password for a user should be automatically generated. Is it the same case for an employee?
[2] How will the system user know their password, via email or a different way?"

> **Answer:**
1- Yes. I already answered this question.
2- I will not answer this question during Sprint B. I will not answer questions that require knowledge that will be introduced later, in the next sprints.


> **Question:** Regarding US3: "As a receptionist, I want to register an SNS User.", What are the necessary components in order to register an SNS User?"

> **Answer:**
The attributes that should be used to describe a SNS user are: Name, Address, Sex, Phone Number, E-mail, Birth Date, SNS User Number and Citizen Card Number.
The Sex attribute is optional. All other fields are required.
The E-mail, Phone Number, Citizen Card Number and SNS User Number should be unique for each SNS user.


> **Question:** "There we can read "US03 - As a receptionist, I want to register a SNS User."
Accordingly to our project description, the person allowed to register a SNS User it's the DGS Administrator".

> **Answer:** There is no error. We will have two User Stories for registering SNS users. One of these USs is US03 and the other US will be introduced later, in Sprint C.


### 1.3. Acceptance Criteria

ACC1: The SNS User must become a system user. The "auth" component available on the repository must be reused (without modifications).

ACC2: The Receptionist should ask for mandatory data (Name, Adress, Phone number, E-mail, Birth Date, SNS User number and Citizen card number).

ACC3: Password is generated randomly.

ACC4: The sex is optional.

ACC5: The data should be unique to each SNS User (Email, Phone number, SNS User number and Citizen Card number).

### 1.4. Found out Dependencies
No dependencies found.

### 1.5 Input and Output Data

Input Data:

>- SNS User number

>- name

>- E-mail

>- Phone number

>- Adress

>- Sex

>- Birth Date

>- Citizen Card Number


Output Data:
>- Showing the data that inserted by the receptionist and ask for confirmation


>- A message informing the acceptance or denial of the SNS User registration in the system


### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![US03-SSD](US03-SSD.svg)


### 1.7 Other Relevant Remarks

The receptionist can register a new SNS User at any time and whenever a new user, who does not exist in the system, wants to.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.* 

![US03-MD](US03-MD.svg)

### 2.2. Other Remarks

n/a

## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1 |	... interacting with the actor? | AddSNSUserUI   |  **Pure Fabrication:**           |
| 			  		 | ... coordinating the US? | AddSNSUserController | **Controller**                             |                               | 
| Step 2 | n/a |             |                              |
| Step 3  | ...saving the typed data? | SNSUser  | **IE:** a SNSUser knows its own data 						 |             |                              |
| 		                 |	... instantiating a new SNS User? | SNSUserSto   | **Creator (R1)** and **HC+LC** |
|  		 			     |  ... knows SNSUserSto?	 |  Company   |  **IE:** Company knows the SNSUserSto because it is delegating some tasks to it |
|  		             |	... validating data (local validation)? | SNSUser | **IE:** an object knows its data|
| 			  		 |	... validating data (global validation)? | SNSUserSto | **IE:** knows all the SNS Users| 
|                    | ... validate the uniqueness of email? | AuthFacade | **IE:** user management is responsibble for the external component whose point of interaction is through "AuthFacade"
| Step 4 |	 |   |  |
| Step 5 |	... generating the password? | PasswordGenerator | **IE** and **Pure Fabrication:** |
| 			  		 |	... registering the client as a system user? | AuthFacade | **IE:**  | 
| 			  		 |	... saving the client? | SNSUserSto | **IE:** Knows all SNSUsers | 		  		
| Step 6 | sending a message showing the confirmation of the operation | UI | **IE:** responsible for user interaction |              



### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Company
 
 * SNS User
 
 

Other software classes (i.e. Pure Fabrication) identified: 

 * AddSNSUserUI  
 
 * AddSNSUserController
 
 * SNSUserSto
 
 * PasswordGenerator

Other software classes of external systems/components:

* AuthFacade

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![US03-SD](US03-SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US03-CD](US03-CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**AddUser_Test 1:** Check that it is not possible to create an instance of the Example class with null values. 

	@AddUser_Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.* 

# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





