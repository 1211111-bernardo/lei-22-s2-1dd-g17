# US 011 - To get a list of Employees with a given function/role.

## 1. Requirements Engineering

### 1.1. User Story Description

>As an administrator, I want to get a list of Employees with a given function/role.

### 1.2. Customer Specifications and Clarifications 

>The user story didn't need clarification provided by the client because the interpretation was quite simple and straightforward. 

### 1.3. Acceptance Criteria

>When as an Administrator using the UI, I want the list of Employees to give the right function/role for each one of them.

### 1.4. Found out Dependencies

>US11 relies on US10(Relation between administrator and employee)

### 1.5 Input and Output Data

>***Input Data***

Function/Role:

- role

>***Output Data***

Employee:
- employeeID
- name 
- address
- phoneNumber
- email
- citizenCardNumber
- role


### 1.6. System Sequence Diagram (SSD)


![US011-SSD](US011-SSD.svg)


### 1.7 Other Relevant Remarks

>Omitted / Not Provided.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US011-DM](US011-DM.svg)

### 2.2. Other Remarks

>Omitted / Not Provided.


## 3. Design - User Story Realization 

### 3.1. Rationale

>Omitted / Not Provided.


## 3.2. Sequence Diagram (SD)


![US011-SD](US011-SD.svg)

## 3.3. Class Diagram (CD)


![US011-CD](US011-CD.svg)

# 4. Tests 

>Omitted / Not Provided. 

# 5. Construction (Implementation)

>Omitted / Not Provided.

# 6. Integration and Demo 

>Omitted / Not Provided.

# 7. Observations

>Omitted / Not Provided.




