# US 04

## 1. Requirements Engineering

*In this section, it is suggested to capture the requirement description and specifications as provided by the client as well as any further clarification on it. It is also suggested to capture the requirements acceptance criteria and existing dependencies to other requirements. At last, identify the involved input and output data and depicted an Actor-System interaction in order to fulfill the requirement.*


### 1.1. User Story Description

 As a receptionist at a vaccination center, I want to register the arrival of a SNS user
to take the vaccine.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> "When the SNS user arrives at the vaccination center, a receptionist registers the arrival of the user to
take the respective vaccine. The receptionist asks the SNS user for his/her SNS user number and
confirms that he/she has the vaccine scheduled for the that day and time. If the information is
correct, the receptionist acknowledges the system that the user is ready to take the vaccine. Then,
the receptionist should send the SNS user to a waiting room where (s)he should wait for his/her
time. "

**From the client clarifications:**

>  **Question:** What attributes are needed in order to register the arrival of a SNS User at the vaccination center?  

> **Answer:** "The time of arrival should be registered."

>  **Question:** 

> **Answer:** 
### 1.3. Acceptance Criteria

 No duplicate entries should be possible for the same SNS
user on the same day or vaccine period.

### 1.4. Found out Dependencies

No dependencies founded.

### 1.5 Input and Output Data

**Input Data**

* Typed data:
    * SNS user number
    * Time of the arrival
    
 **Output Data:**

* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)


![US04-SSD](US04-SSD.svg)


### 1.7 Other Relevant Remarks




## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US04-MD](US04-MD.svg)

### 2.2. Other Remarks


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1: Want to register the arrival of a SNS User         | interacting with the actor?    | ArrivalUI       |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.                            |
| 		                                                     | instantiating a new arrival?	  | Company         |  Creator                                           |
| Step 2: Request Data(SNS User Number) 		             |			n.a				      |                 |                                                    |
| Step 3: Types the requested data  		                 | saving the input data?		  | ArrivalUser     | IE: object created in step 1 has its own data.     |
| Step 4: Shows the typed data and asks for confirmation     | 	show the data?				  | ArrivalUI       |                                                    |
| Step 5: Confirms the data                                  | 	validating the data?	      | ArrivalStore    | IE:Knows all authenticaded data                    |
|                                                            | 	saving the arrived user?	  | Company         | IE:Knows all authenticaded data                    |
| Step 6:Informs the operation sucess  		                 | 	informing operation success?  |  ArrivalUI      |  IE: is responsible for user interactions          |



### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Company
 * ArrivalUser

Other software classes (i.e. Pure Fabrication) identified: 
 * ArrivalUI  
 * ArrivalController

## 3.2. Sequence Diagram (SD)

![US04-SD](US04-SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US04-CD](US04-CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**AddUser_Test 1:** Check that it is not possible to create an instance of the Example class with null values. 

	@AddUser_Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.* 

# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





