import app.domain.Store.AdministrateVaccineStore;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AdministrateVaccineStoreTest {

    @Test
    void CounterShouldEqual5(){
        var store=new AdministrateVaccineStore();
        store.addAdministrateVaccine("12344143", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("123141334", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("34131234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("3141341234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("131413234", "Pfizer","Covid", "50");
        assertEquals(5,store.CounterArrayAdministrate());
    }
    @Test
    void CounterShouldEqual0(){
        var store=new AdministrateVaccineStore();
        assertEquals(0,store.CounterArrayAdministrate());
    }
    @Test
    void CounterShouldEqual20(){
        var store= new AdministrateVaccineStore();
        store.addAdministrateVaccine("12344143", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("123141334", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("34131234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("3141341234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("131413234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("123441430", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("1231413340", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("341312340", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("31413412340", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("1314132340", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("102344143", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("1230141334", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("341031234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("31401341234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("13010413234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("1203044143", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("12301041334", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("3041301234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("310401341234", "Pfizer","Covid", "50");
        store.addAdministrateVaccine("0131413234", "Pfizer","Covid", "50");
        assertEquals(20,store.CounterArrayAdministrate());
    }

}