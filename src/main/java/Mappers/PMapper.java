package Mappers;

import app.domain.shared.PerformanceCenter;
import dto.PerformanceDTO;

public class PMapper {
    private String Center;
    private String Date;
    private String m;

    /**
     * Constructor that allows to instantiate a new PerformanceCenter
     * @param Center corresponds to the center that the programme will analyze
     * @param Date corresponds to the date that the center will be analyzed
     * @param TimeInterval corresponds to the time interval of the analysis
     */
    public PMapper (String Center,String Date,String TimeInterval) {
        this.Center = Center;
        this.Date = Date;
        this.m = TimeInterval;
    }
    /**
     * Constructor that allows to instantiate a new PerformanceCenter
     * @param Date corresponds to the date that the center will be analyzed
     * @param TimeInterval corresponds to the time interval of the analysis
     */
    public PMapper (String Date,String TimeInterval) {
        this.Date = Date;
        this.m = TimeInterval;
    }

    /**
     * Thid method instantiates a new PerformanceDTO
     * @param Date corresponds to the date that the center will be analyzed
     * @param TimeInterval corresponds to the time interval of the analysis
     */
    public void ToModel (String Date,String TimeInterval) {
        PerformanceDTO dto = new PerformanceDTO(Date,TimeInterval);
        this.Date = dto.getDate();
        this.m = dto.getTimeInterval();
    }

    /**
     * Thos method creates a new PerformanceCenter
     * @param dto - contains all data necessary to create a new PerformanceCenter
     */
    public void create(PMapper dto) {
        PerformanceCenter PC = new PerformanceCenter(Date,m);
    }
}
