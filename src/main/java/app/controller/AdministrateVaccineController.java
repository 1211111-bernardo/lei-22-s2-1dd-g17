package app.controller;

import app.domain.Store.AdministrateVaccineStore;
import app.domain.Store.SNSUserSto;
import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.shared.AdministrateVaccine;
import dto.AdministrateVaccineDTO;
import dto.ScheduleVaccineDTO;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

public class AdministrateVaccineController {

    /**
     * The AgeCalculator method is responsible to calculate the age of the SNS User
     * Gets by parameter the position of the user in the SNS User List
     * After the method calculates the period between the birthdate and the today`s date it should print the result
     */
    public void AgeCalculator(int i) {
        SNSUserSto SUSs = new SNSUserSto();
        ArrayList<SNSUser> snsUserList = SUSs.getSnsUserList();
        String Birthday = snsUserList.get(i).getBirthDate();
        String[] parts = Birthday.split("/");
        String Day = parts[0];
        String Month = parts[1];
        String Year = parts[2];
        int Dayint =Integer.parseInt(Day);
        int Monthint =Integer.parseInt(Month);
        int Yearint =Integer.parseInt(Year);
        LocalDate today = LocalDate.now();
        LocalDate birthdate = LocalDate.of(Yearint, Monthint, Dayint);

        Period p = Period.between(birthdate, today);

        System.out.println(p.getYears());
    }

    /**
     * Returns snsuserList
     * @return snsuserlist
     */
    public static ArrayList<SNSUser> getSnsUserList() {
        ScheduleVaccineDTO dto= new ScheduleVaccineDTO();
        return dto.getSnsUserList();
    }
    /**
     * Returns SnsUserStore
     * @return SnsUserSto
     */
    public void getSnsUserStore(){
    }

    /**
     * Gets by parameter all data required to register the administration of a vaccine
     * Creates new Company with the same data
     * @param dto has all required date
     */
    public void createAdministrateVaccine(AdministrateVaccineDTO dto) {
        Company AdministrateVaccine = new Company(dto);
    }

    /**
     * Gets by parameter all data required to register the administration of a vaccine
     * Creates new Company with the same data
     * @param avstore has all required date (SNS Number, Time)
     */
    public void saveAdministrateVaccine(AdministrateVaccine avstore ){
        AdministrateVaccineStore avsto = new AdministrateVaccineStore();
    }
}

