package app.controller;

import app.domain.Store.ArrivalStore;
import app.domain.model.Company;
import dto.AUserDto;

public class ArrivalController {
    /**
     * Creates new ArrivalStore object
     * Uses ArrivalStore getTime method to get the local time
     * @return present time
     */
    public String getTime() {
        ArrivalStore AS = new ArrivalStore();
        return AS.getTime();
    }
    /**
     * Gets by parameter all data required to register a new arrival user
     * Creates new Company with the same data
     * @param dto has all required date (SNS Number, Time)
     */
    public void createArrivalUser(AUserDto dto) {
        Company AUser = new Company(dto);
    }

    /**
     * Gets by parameter all data required to register a new arrival user
     * @param snsNumber
     * @param time
     * @return String representing in(sucess) of the operation
     */
    public String saveArrival(String snsNumber, String time) {
        ArrivalStore Auserstore = new ArrivalStore(snsNumber,time);
        return Auserstore.saveArrival(snsNumber,time);
    }

    /**
     * Controller method that calls ArrivalStore Class to get all data in form of string
     */
    public void tostring() {
        ArrivalStore ts = new ArrivalStore();
        ts.tostring();
    }

    /**
     * !!! THE FOLLOWING 2 METHODS ARE NOT PERMANENT !!!
     * !!! They are used to help the group check if the data are being registed in a correct way !!!
     */
    public void setVaccinationCenter (String vaccinationcenter) {
        ArrivalStore setcenter = new ArrivalStore();
        setcenter.setcenter(vaccinationcenter);
    }
    public String getVaccinationCenter () {
        ArrivalStore getcenter = new ArrivalStore();
       return getcenter.getcenter();
    }
}
