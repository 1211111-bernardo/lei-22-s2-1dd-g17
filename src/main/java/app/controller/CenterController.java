package app.controller;

import app.domain.Store.CenterStore;
import app.ui.console.utils.Utils;

public class CenterController {
    public String createCenter (String name) {
        CenterStore center = new CenterStore(name);
        return center.SaveStore(name);
    }
}
