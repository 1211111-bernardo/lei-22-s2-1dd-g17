package app.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import app.domain.Store.AdministrateVaccineStore;
//import app.domain.model.Administration;
import app.domain.model.Company;
import app.domain.Store.AdministrateVaccineStore;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ListAdministrationsController {
    private Company company;
    private AdministrateVaccineStore store;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private DatePicker lastDate;

    @FXML
    private DatePicker firstDate;

    @FXML
    private TextArea writeList;

    @FXML
    private Button listButton;

    FileChooser fileChooser = new FileChooser();

    @FXML
    void getVaccineAdministrationList(ActionEvent event) throws IOException {
        LocalDate before = firstDate.getValue();
        LocalDate after = lastDate.getValue();

        if(before == null || after == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Dates Empty");
            alert.setContentText("At least one of the dates is empty please try again");
            alert.showAndWait();
        } else if (before.isAfter(after)){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong Dates Inserted");
            alert.setContentText("The initial date is after the last date, insert again");
            alert.showAndWait();
        }else{
           // String text = this.store.getListAdministrations(before,after);
           // writeList.setText(text);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Export to CSV");
            alert.setContentText("Do you wish to export this information to an CSV file?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                // Set extension filter
                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV file (*.csv)", "*.csv");
                fileChooser.getExtensionFilters().add(extFilter);
                File file = fileChooser.showSaveDialog(new Stage());
                if(file != null){
                    if(file.createNewFile()){
                      //  store.generateCSV(text,file);
                    }
                }
            }

        }





    }

    @FXML
    void initialize() {
        this.company = App.getInstance().getCompany();
        assert lastDate != null : "fx:id=\"LastDate\" was not injected: check your FXML file 'listAdministrations.fxml'.";
        assert firstDate != null : "fx:id=\"FirstDate\" was not injected: check your FXML file 'listAdministrations.fxml'.";
        assert writeList != null : "fx:id=\"WriteList\" was not injected: check your FXML file 'listAdministrations.fxml'.";
        assert listButton != null : "fx:id=\"listButton\" was not injected: check your FXML file 'listAdministrations.fxml'.";
        this.store = this.company.getAdministrateVaccineStore();
    }
}