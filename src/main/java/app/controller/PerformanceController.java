package app.controller;

import Mappers.PMapper;
import app.domain.Store.PerformanceStore;

import java.io.FileNotFoundException;
import java.text.ParseException;

public class PerformanceController {
    /**
     * Gets by parameter all data needed to create a new performance center analyze
     * @param center corresponds to the center that the programme will analyze
     * @param date corresponds to the date that the center will be analyzed
     * @param m corresponds to the time interval of the analysis
     */
    public void createPerformanceCenter(String center,String date,String m) {
        PerformanceStore PC = new PerformanceStore(center,date,m);
        PC.createPerformanceStore(center,date,m);

    }
    /**
     * Gets by parameter data needed to save a performance center analyze
     * @param Date corresponds to the date that the center will be analyzed
     * @param m corresponds to the time interval of the analysis
     */
    public void savePerformanceCenter(String Date,String m) throws FileNotFoundException, ParseException {
       PerformanceStore PC = new PerformanceStore(Date,m);
       PC.savePerformanceCenter(Date,m);
    }
}
