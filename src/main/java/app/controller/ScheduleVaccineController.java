package app.controller;

import app.domain.Store.ArrivalStore;
import app.domain.Store.SNSUserSto;
import app.domain.Store.ScheduleVaccineStore;
import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.shared.ArrivalUser;
import app.domain.shared.IDRole;
import app.domain.shared.ScheduleVaccine;
import dto.AUserDto;
import dto.ScheduleVaccineDTO;
import dto.WaitingRoomDto;

import java.util.ArrayList;

public class ScheduleVaccineController {
    private Company company;
    private ScheduleVaccine sv;
    private SNSUserSto SnsUserNumber;


    /**
     * Gets by parameter SNS User Number
     */
    public ScheduleVaccineController (String SnsUserNumber){
    }

    public ScheduleVaccineController() {
    }

    /**
     * Returns snsuserList by instantiating ScheduleVaccineDTO
     * @return snsuserlist
     */
    public static ArrayList<SNSUser> getSnsUserList() {
        ScheduleVaccineDTO dto= new ScheduleVaccineDTO();
        return dto.getSnsUserList();
    }

    /**
     * Allows the system to change/set a new SnsUSerNhumber
     * @param SnsUserNumber
     */
    public void setSnsUserNumber (String SnsUserNumber) {
        ScheduleVaccineStore setcenter = new ScheduleVaccineStore();
        setcenter.setcenter(SnsUserNumber);
    }
    /**
     * Allows the system to change/set a new vaccinationCenter
     * @param vaccinationcenter
     */
    public void setVaccinationCenter (String vaccinationcenter) {
        ScheduleVaccineStore setcenter = new ScheduleVaccineStore();
        setcenter.setcenter(vaccinationcenter);
    }

    /**
     * Allows the system to change/set a new Time
     * @param Time
     */
    public void setTime (String Time) {
        ScheduleVaccineStore setcenter = new ScheduleVaccineStore();
        setcenter.setcenter(Time);
    }

    /**
     * Allows the system to change/set a new Date
     * @param Date
     */
    public void setDate (String Date) {
        ScheduleVaccineStore setcenter = new ScheduleVaccineStore();
        setcenter.setcenter(Date);
    }
    /**
     * Allows the system to change/set a new Vaccine
     * @param Vaccine
     */
    public void setVaccine (String Vaccine) {
        ScheduleVaccineStore setcenter = new ScheduleVaccineStore();
        setcenter.setcenter(Vaccine);
    }
    public void createScheduleVaccine(ScheduleVaccineDTO dto) {
        Company VaccineAppointment = new Company(dto);
    }

}