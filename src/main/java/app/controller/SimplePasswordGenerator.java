package app.controller;

import java.util.Random;

public class SimplePasswordGenerator {
    private static final String STRING_BASE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?@#$%&/()[]{}<>*-+._-:,;";
    private static int DEFAULT_LEN=12;
    private int len;

    public SimplePasswordGenerator(int len) {
        this.len = len;
    }

    public SimplePasswordGenerator() {
        this.len=DEFAULT_LEN;
    }


    public String generatePassword() {
        StringBuilder sb = new StringBuilder();
        int max=STRING_BASE.length()-1;
        for (int i = 0; i < len; i++) {
            int index = ((int) Math.round(Math.random() * max));
            sb.append(STRING_BASE.charAt(index));
        }
        return sb.toString();
    }


}
