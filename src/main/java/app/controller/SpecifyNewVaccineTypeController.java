package app.controller;

import app.domain.Store.VaccineTypeStore;
import app.domain.model.Company;
import app.domain.shared.VaccineType;

public class SpecifyNewVaccineTypeController {
    private Company company;
    private VaccineType vt;
    public SpecifyNewVaccineTypeController() {
        this(App.getInstance().getCompany());
    }
    public SpecifyNewVaccineTypeController(Company company) {
        this.company = company;
        this.vt = null;
    }


    public boolean createVaccineType(String code, String designation, String whoId) {
        this.vt = this.company.createVaccineType(code, designation, whoId);
        return this.company.validateVaccineType(vt);
    }

    public void addVaccineType(String code, String designation, String whoId) {
        VaccineTypeStore vt = new VaccineTypeStore(code, designation, whoId);
    }
}
