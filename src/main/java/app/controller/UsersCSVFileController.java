package app.controller;

import app.domain.Store.SNSUserSto;
import app.domain.model.Company;
import app.domain.shared.Constants;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class UsersCSVFileController {
    private Company company;
    private SNSUserSto user;

    /**
     * Instantiates a new Company.
     */
    public UsersCSVFileController() {
        company = App.getInstance().getCompany();
        this.user = new SNSUserSto();
    }

    /**
     * Calls a method from UsersStore to create all the users and save them on the store.
     *
     * @param file the file path/name
     */
    public boolean saveUsersData(String file) throws FileNotFoundException {
        File f = new File(file);
        String spliter = findSpliter(file);
        if (!f.exists()) {
            throw new FileNotFoundException("There is no file with such path!");
        }

        try (Scanner sc = new Scanner(f)) {
            while (sc.hasNext()) {
                try {
                    String[] line = sc.nextLine().split(spliter);
                    String name = line[0];
                    String sex = line[1];
                    String birthday= line[2];
                    String adress = line[3];
                    String phoneNumber = line[4];
                    String email = line[5];
                    String snsNumber = line[6];
                    String ccNumber = line[7];
                    String password = App.getInstance().getSimplePasswordGenerator().generatePassword();
                    App.getInstance().getCompany().getSNSUserSto().saveSNSUser(name,email,adress,phoneNumber,birthday,snsNumber,ccNumber, sex);
                    App.getInstance().getCompany().getSNSUserSto().addSNSUser(name,email,password,Constants.ROLE_SNS_USER);
                } catch (NumberFormatException e) {
                    continue;
                }

            }
        }
        return true;
    }

    private String findSpliter(String file) throws FileNotFoundException {
        File f = new File(file);
        Scanner sc = new Scanner(f);
        String s = sc.nextLine();
        if (s.matches(".*;.*")){
            return ";";
        }
        return ",";
    }
}

