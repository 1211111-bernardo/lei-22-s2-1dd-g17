package app.controller;

import app.domain.Store.WaitingRoomStore;
import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.shared.ArrivalUser;
import dto.WaitingRoomDto;

import java.util.ArrayList;

public class WaitingRoomController {

    private ArrayList<SNSUser> snsUserList;
    private ArrayList<ArrivalUser>ARRIVAL_USER_ARRAY_LIST;

    /**
     * Gets by parameter all the required data
     * Creates company with required data
     * @param dto
     */
    public void createWaiting(WaitingRoomDto dto) {
        Company room = new Company(dto);
    }

    /**
     * method responsible for controlling the activity of method createwaitinglist in WaitingRoomStore
     * Gets by parameter 2 arraylists to create a new waitinglist
     * @param ARRIVAL_USER_ARRAY_LIST
     * @param snsUserList
     */
    public void createWaitingList(ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST, ArrayList<SNSUser> snsUserList) {
        WaitingRoomStore sto = new WaitingRoomStore(ARRIVAL_USER_ARRAY_LIST,snsUserList);
        sto.createWaitingList(ARRIVAL_USER_ARRAY_LIST,snsUserList);
    }

    /**
     * Method responsible for controlling the activity of the method printarray in WaitingRoomStore
     * Gets by parameter the waitinglist to show/print in the UI
     * @param WaitingList
     */
    public void printarray(ArrayList<SNSUser> WaitingList) {
        WaitingRoomStore ts= new WaitingRoomStore();
        ts.printarray(WaitingList);
    }

    /**
     * Allows the system to change/set a new vaccinationCenter
     * @param vaccinationcenter
     */
    public void setVaccinationCenter (String vaccinationcenter) {
        WaitingRoomStore setcenter = new WaitingRoomStore();
        setcenter.setcenter(vaccinationcenter);
    }

    /**
     * Returns a center by instantiating WaitingRoomStore
     * @return center
     */
    public String getVaccinationCenter () {
        WaitingRoomStore getcenter = new WaitingRoomStore();
        return getcenter.getcenter();
    }

    /**
     * Returns snsuserList by instantiating WaitingRoomDto
     * @return snsuserlist
     */
    public ArrayList<SNSUser> getSnsUserList() {
        WaitingRoomDto dto= new WaitingRoomDto();
        return dto.getSnsUserList();
    }

    /**
     * Returns arrivaluserarraylist by instantiating WaitingRoomDto
     * @return arrivaluserarraylist
     */
    public ArrayList<ArrivalUser> getARRIVAL_USER_ARRAY_LIST() {
        WaitingRoomDto dto= new WaitingRoomDto();
        return dto.getARRIVAL_USER_ARRAY_LIST();
    }

    /**
     * Returns WaitingList by instantiating WaitingRoomStore, where this arraylist is saved and ready to be used
     * @return WaitingList
     */
    public  ArrayList<SNSUser> getWaitingList(){
        WaitingRoomStore sto= new WaitingRoomStore();
        return sto.getWaitingList();
    }

}
