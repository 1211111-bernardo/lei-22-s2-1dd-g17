package app.domain.Store;

import app.controller.App;
import app.domain.shared.AdministrateVaccine;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class AdministrateVaccineStore {
    /**
     * This Array List is responsible for saving all the Administrated Vaccines with the respetive information
     */
    final static ArrayList<AdministrateVaccine> AdministrateVaccines = new ArrayList<>();



    /**
     * This method is responsible for adding a new Vaccine Administration
     * Gets by parameter all data required to register a new Vaccine Administration
     * @param SnsUserNumber
     * @param Vaccine
     * @param Dose
     * @param Type
     */

    public void addAdministrateVaccine(String SnsUserNumber, String Type, String Vaccine, String Dose) {
        AdministrateVaccine av = new AdministrateVaccine(SnsUserNumber, Type, Vaccine, Dose);
        AdministrateVaccines.add(av);
    }

    public int CounterArrayAdministrate(){
        int counter=0;
        for (int i = 0; i < AdministrateVaccines.size(); i++) {
            counter++;
        }
        return counter;
    }

    public void removeUserFromCentre() {
        int counter = 0;
        for (int i = 0; i < AdministrateVaccines.size(); i++) {
            AdministrateVaccines.remove(i);
        }
    }

    public int getArraySize(){
        int size= AdministrateVaccines.size();
        return size;
    }

    public void generateCSV(String text, File file) throws IOException {
        FileWriter myWriter = new FileWriter(file);
        myWriter.write(text);
        myWriter.close();
    }



}
