package app.domain.Store;

import app.domain.shared.ArrivalUser;
import app.domain.shared.ScheduleVaccine;
import dto.AUserDto;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Date;

public class ArrivalStore implements Serializable {
    static ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST = new ArrayList<>();
    final static ArrayList<String> Centro_Vacinaçao = new ArrayList<>();
    /**When called this method automatically read the ARRIVAL_USER_ARRAY_LIST
     */
    public void readFicheiroBinário () {
        try {
            FileInputStream file = new FileInputStream("C:\\Users\\berna\\Desktop\\Performance Center analyze data\\ArrivalStore.txt");
            ObjectInputStream in = new ObjectInputStream(file);
            ARRIVAL_USER_ARRAY_LIST = (ArrayList<ArrivalUser>) in.readObject();
            in.close();
            file.close();
        }catch (IOException | ClassNotFoundException ex){
            System.out.println("Some error occur during the file read!");
        }
    }
    /**
     * When called this method automatically saves the ARRIVAL_USER_ARRAY_LIST
     * @param ARRIVAL_USER_ARRAY_LIST
     * @return true or false depending on operation success
     */
    public boolean guardarFicheiroBinario (ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST) {
        return guardarFicheiroBinario(new File("C:\\Users\\berna\\Desktop\\Performance Center analyze data\\ArrivalStore.txt"),ARRIVAL_USER_ARRAY_LIST);
    }
    /**
     *
     * @param ficheiro file name that contains the saved data
     * @param ARRIVAL_USER_ARRAY_LIST - saved array list
     * @return true or false depending on operation success
     */
    public boolean guardarFicheiroBinario(File ficheiro,ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(ficheiro));
        try { out.writeObject(ARRIVAL_USER_ARRAY_LIST);
            return true;
        }finally {
                out.close();
            }
        }
        catch (IOException e) {
            return false;
        }
    }
    /**
     * Method used to instantiate new ArrivalStore object without any parameters
     */
    public ArrivalStore () {
    }

    /**
     * Method used to instantiate new ArrivalStore object
     * @param number - SNS Number
     * @param time - Time of the arrival of SNS User
     */
    public ArrivalStore (String number,String time) {
    }

    /**
     * Gets by parameter all data required to register a new arrival user
     * Creates new AttivalStore object with the same data
     * @param dto has all required date (SNS Number, Time)
     */
    public ArrivalStore (AUserDto dto) {
        String SNSNumber = dto.getSNSNumber();
        String Time = dto.getTime();
        ArrivalUser AUser = new ArrivalUser(SNSNumber,Time);
    }


/**
 *  Gets by parameter an array list
 *  This array list contains the list of arrivals, while we don´t have information about how select specific users that have
 already left the waiting room
 * @param Waitinglist is the list of users in the WaitingRoom
 *
 *            Usar calendar para fazer diferença entre horas e poder remover os utilizadores da WaitingRoom     !!!!!
 *
 *
   */
    public ArrivalStore(ArrayList<ArrivalUser> Waitinglist) {
        Waitinglist= ARRIVAL_USER_ARRAY_LIST;
    }

    /**
     * Allows programme to get the current time when the method is called
     * @return current local time
     */
    public String getTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    /**
     * Saves the created ArrivalUser validating all data
     * @param snsNumber
     * @param time
     * @return String representing the (in)sucess of the operation
     */
    public String saveArrival(String snsNumber, String time) {
        readFicheiroBinário();
        if (validateArrivalUser(snsNumber,time).equals("1")) {
            return "Unscheduled SNS User";
        }
        if (validateArrivalUser(snsNumber,time).equals("2")) {
            return "User has to wait at least 24 hours to return";
        } else
            addArrivalUser(snsNumber,time);
        guardarFicheiroBinario(ARRIVAL_USER_ARRAY_LIST);
        return "Arrival registed with success";

    }

    /**
     * Method used to validate all ArrivalUser data comparing flag values
     * flag = 0 means that all data is correct
     * flag = 1 means that the register ArrivalUser is not scheduled
     * flag = 2 means that the current ArrivalUser was register in less than 24 hours
     * @param snsNumber
     * @param time
     * @return flag as String with the respective values
     */
    public String validateArrivalUser (String snsNumber, String time) {
        int flag = 0;
       if (CompareUSerNumber(snsNumber) == false){
          flag = 1;
       } else
       if (CompareTime(time)== false){
           flag = 2;
       }
       String Validation = Integer.toString(flag);
       return Validation;
    }

    /**
     * Gets by parameter SNSNumber and it compares with all SNSNumbers register in the schedule user array list
     * @param number
     * @return true or false depending on validation (in)success
     */
    public boolean CompareUSerNumber(String number) {
        ScheduleVaccineStore SVs = new ScheduleVaccineStore();
        int flag = 0;
        for (int i = 0; i < SVs.getVaccineAppointments().size();i++) {
            if (SVs.getVaccineAppointments().get(i).getSnsUserNumber().equals(number)) {
                flag = 1;
            }
        }
        if (flag == 0) {
            return false;
        } else return true;
    }

    /**
     * Gets by parameter the time of the ArrivalUser Arrive and compares the user time between arrival registrations
     * @param time -Time of the arrival of SNS User
     * @return true or false depending on (in)success of the operation
     */
    public boolean CompareTime(String time) {
        int p = 0;
        // Gets existing or not duplicated ArrivalUser in the arraylist ARRIVAL_USER_ARRAY_LIST
        for (int i = 0; i < ARRIVAL_USER_ARRAY_LIST.size();i++) {
            if (ARRIVAL_USER_ARRAY_LIST.get(i).getTime().equals(time)) {
                p = i;
            }
        }
        // SimpleDateFormat converts the string format to date object
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            // parse method is used to parse the text from a string to produce the date
            Date d1 =sdf.parse(ARRIVAL_USER_ARRAY_LIST.get(p).getTime());
            Date d2 =sdf.parse(ARRIVAL_USER_ARRAY_LIST.get(p).getTime());
            //Calculate time difference
            long difference_in_time = d2.getTime() - d1.getTime();
            long difference_in_hours =  (difference_in_time / (1000*60*60)) % 24;
            if (difference_in_hours > 24) {
                return false;
            }
            //Catch the exception
        } catch (IndexOutOfBoundsException | ParseException e) {
            return true;
        }
        return true;
    }

    /**
     * Gets by parameter all data required to register a new arrival user
     * @param snsNumber
     * @param time
     */
    public void addArrivalUser (String snsNumber, String time) {
        ARRIVAL_USER_ARRAY_LIST.add(new ArrivalUser(snsNumber,time));
    }

    /**
     * Method used to check if ArrivalUsers are being saved in the arraylist
     */
    public void tostring () {
        for (int i = 0; i < ARRIVAL_USER_ARRAY_LIST.size();i++) {
            System.out.printf("User %d  ",i);
            System.out.printf("%s    ",ARRIVAL_USER_ARRAY_LIST.get(i).getSNSNumber());
            System.out.printf("%s\n",ARRIVAL_USER_ARRAY_LIST.get(i).getTime());
        }
    }
    /**
     * !!! THE FOLLOWING 2 METHODS ARE NOT PERMANENT !!!
     * !!! They are used to help the group check if the data are being registed in a correct way !!!
     */
    public void setcenter(String vaccinationcenter) {
        Centro_Vacinaçao.add(vaccinationcenter);
    }
    public String getcenter () {
        return Centro_Vacinaçao.get(0);
    }

    public ArrayList<ArrivalUser> getArrivalUserArrayList() {
        return ARRIVAL_USER_ARRAY_LIST;
    }
}
