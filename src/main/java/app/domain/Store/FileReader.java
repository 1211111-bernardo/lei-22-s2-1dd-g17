package app.domain.Store;

import app.domain.shared.PerformanceCenter;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileReader {
    final static ArrayList<PerformanceCenter> PerformanceArrayList = new ArrayList<>();

    /**
     * This method reads the file and keeps the necessary data to analyze the performance of a center
     * @throws FileNotFoundException in case the pathname is invalid
     */
    public FileReader () throws FileNotFoundException {
       File f = new File("C:\\Users\\berna\\Desktop\\Performance Center analyze data\\performanceDataFromGaoFuNationalCenterDoPortoVaccinationCenter (4).csv");
        String spliter = Spliter();
        if (!f.exists()) {
            throw new FileNotFoundException("There not founded!");
        }
        try (Scanner sc = new Scanner(f)){
            while (sc.hasNext()) {
                try {
                    String[] line = sc.nextLine().split(spliter);
                    String ArrivalTime = line[5];
                    String LeaveTime = line[7];
                    PerformanceArrayList.add(new PerformanceCenter(ArrivalTime,LeaveTime,null));
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("Invalid data ");
                }
            }
        }
    }
    public ArrayList<PerformanceCenter> getArrayList() {
        return PerformanceArrayList;
    }

    /**
     * @return the spliter of the file
     * @throws FileNotFoundException
     */
    private String Spliter() throws FileNotFoundException {
        File f = new File("C:\\Users\\berna\\Desktop\\Performance Center analyze data\\performanceDataFromGaoFuNationalCenterDoPortoVaccinationCenter (4).csv");
        Scanner sc = new Scanner(f);
        String s = sc.nextLine();
        if (s.matches(".*;.*")){
            return ";";
        }
        return ",";
    }
}
