package app.domain.Store;

import Mappers.PMapper;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Formatter;

public class PerformanceStore {
    int[] nums = new int[720];

    /**
     * Constructor that allows to instantiate a new PerformanceCenter
     * @param dto that contains all necessary data to process the wanted data
     */
    public PerformanceStore(PMapper dto) {
    }

    /**
     * Constructor that allows to instantiate a new PerformanceCenter
     * @param center corresponds to the center that the programme will analyze
     * @param date corresponds to the date that the center will be analyzed
     * @param m corresponds to the time interval of the analysis
     */
    public PerformanceStore(String center, String date, String m) {

    }
    /**
     * Constructor that allows to instantiate a new PerformanceCenter
     * @param date corresponds to the date that the center will be analyzed
     * @param m corresponds to the time interval of the analysis
     */
    public PerformanceStore(String date, String m) {
    }

    /**
     * This method allows the creation of a new performance center using the PMapper class
     * @param center corresponds to the center that the programme will analyze
     * @param date corresponds to the date that the center will be analyzed
     * @param m corresponds to the time interval of the analysis
     */
    public void createPerformanceStore(String center, String date, String m) {
        PMapper dto = new PMapper(date, m);
        dto.ToModel(date, m);
        dto.create(dto);
    }

    /**
     * This method gives acess to all calculated data if the input validation succeed
     * @param date corresponds to the date that the center will be analyzed
     * @param m corresponds to the time interval of the analysis
     * @throws FileNotFoundException if the data file is not founded
     * @throws ParseException if the read date if invalid
     */
    public void savePerformanceCenter(String date, String m) throws FileNotFoundException, ParseException {
        ValidatePerformanceCenter(date,m);
        System.out.printf("Date: %s \n",date);
        int TimeInterval = Integer.parseInt(m);
        System.out.printf("Interval Time: %s minutes \n",m);
        System.out.println("InputList:");
        for (int i = 0; i < 720 / TimeInterval;i++) {
        System.out.println(nums[i]);}
        System.out.println("Maximum Sum:");
        long startTime = System.nanoTime();
        System.out.println(CalculateMaxSum(nums));
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.printf(" %d mili",duration);
    }

    /**
     * This method runs all data and fills the nums int[] list with the diffrence between the arrival number of users and the ones that leave in a given interval time
     * @param Date corresponds to the date that the center will be analyzed
     * @param m corresponds to the time interval of the analysis
     * @throws FileNotFoundException if the data file is not founded
     * @throws ParseException if the read date if invalid
     */
    public void ValidatePerformanceCenter(String Date, String m) throws FileNotFoundException, ParseException {
        FileReader PerformanceArraylist = new FileReader();
        if (CompareDate(Date)) {
            int inicialhour = 800;
            int M = Integer.parseInt(m);
            int totalarrival = 0;
            int totalleave = 0;
            SimpleDateFormat hour = new SimpleDateFormat("M/dd/yyyy HH:mm");
            Formatter fmt = new Formatter();
            totalarrival = inicialhour;
            totalleave = inicialhour + M;
            try {
                for (int f = 0; f < 720 / M; f++) {
                    int ArrivalCont = 0;
                    int LeaveCont = 0;
                    for (int i = 1; i < PerformanceArraylist.getArrayList().size(); i++) {
                        java.util.Date Arrivalhour = hour.parse(PerformanceArraylist.getArrayList().get(i).getArrivalTime());
                        java.util.Date Leavehour = hour.parse(PerformanceArraylist.getArrayList().get(i).getLeavingTime());
                        String ah = String.format("%02d%02d", Arrivalhour.getHours(), Arrivalhour.getMinutes());
                        String lh = String.format("%02d%02d", Leavehour.getHours(), Leavehour.getMinutes());
                        int Ah = Integer.parseInt(ah);
                        int Lh = Integer.parseInt(lh);
                        if (Ah < (totalleave) && Ah > (totalarrival)) {
                            ArrivalCont = ArrivalCont + 1;
                        }
                        if (Lh < (totalleave) && Lh > (totalarrival)) {
                            LeaveCont = LeaveCont + 1;
                        }
                    }
                    totalarrival = totalarrival + M;
                    totalleave = totalleave + M;
                    if ((totalleave - 60) % 100 == 0) {
                        totalleave = totalleave + 40;
                    }
                    if ((totalarrival - 60) % 100 == 0) {
                        totalarrival = totalarrival + 40;
                    }

                        nums[f] = ArrivalCont - LeaveCont;

                }
            } catch (ParseException e) {
                System.out.println("Some error occurred during the date validation ");
            }
        }
    }

    /**
     * This method compares a given date to the dates read in the file
     * @param Date corresponds to the date that the center will be analyzed
     * @return true or false depending on if the dates are equal or not
     * @throws FileNotFoundException if the data file is not founded
     * @throws ParseException if the read date if invalid
     */
    public boolean CompareDate(String Date) throws FileNotFoundException, ParseException {
       /* FileReader filereader = new FileReader();
        int flag = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("M-dd-yyyy HH:mm:ss");
        SimpleDateFormat day = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date d1 = sdf.parse(filereader.getArrayList().get(1).getArrivalTime());
        Calendar calender = Calendar.getInstance();
        calender.setTime(d1);
        String strDate = day.format(d1);
        for (int i = 1; i < filereader.getArrayList().size(); i++) {
            if (strDate.equals(Date)) {
                return true;
            }
        }return false;

        */
        return true;
    }

    /**
     * This method calculates the max sum od a given number list
     * @param nums corresponds to an integer list
     * @return the max sum
     */
        public int CalculateMaxSum(int[] nums) {

                int n = nums.length;
                int maximumSubArraySum = Integer.MIN_VALUE;
                int start = 0;
                int end = 0;

                for (int left = 0; left < n; left++) {

                    int runningWindowSum = 0;

                    for (int right = left; right < n; right++) {
                        runningWindowSum += nums[right];

                        if (runningWindowSum > maximumSubArraySum) {
                            maximumSubArraySum = runningWindowSum;
                            start = left;
                            end = right;
                        }
                    }
                }
                return maximumSubArraySum;
        }

    /**
     * This method returns the integer list
     * @return the nums int[] list
     */
    public int[] getInputList() {
           return nums;
        }
    }

