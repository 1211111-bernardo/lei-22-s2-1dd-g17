package app.domain.Store;

import app.domain.model.SNSUser;
import app.domain.shared.ArrivalUser;
import app.domain.shared.ScheduleVaccine;
import app.ui.console.SNSUserUI;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SNSUserSto extends SNSUserUI {

    final static ArrayList<SNSUser> snsUserList= new ArrayList<>();

    public ArrayList<SNSUser> getSnsUserList() {
        return snsUserList;
    }
    public SNSUserSto () {

    }

    public SNSUserSto(String name, String email, String adress, String PhoneNumber, String BirthDate, String SNSUserNumber, String CitizenCardNumber, String Sex){
        SNSUser user= new SNSUser(name, email, adress, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber, Sex);
    }

    public SNSUserSto(String name, String email, String adress, String PhoneNumber, String BirthDate, String SNSUserNumber, String CitizenCardNumber){
        SNSUser user= new SNSUser(name, email, adress, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber);
    }

    public void saveSNSUser (String name, String email,String adress, String PhoneNumber, String BirthDate, String SNSUserNumber, String CitizenCardNumber){
        SNSUser user= new SNSUser(name,email, adress, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber);
        snsUserList.add(user);
        guardarFicheiroBinario(snsUserList);
    }

    public void saveSNSUser (String name, String email,String adress, String PhoneNumber, String BirthDate, String SNSUserNumber, String CitizenCardNumber, String sex){
        SNSUser user= new SNSUser(name,email, adress, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber,sex);
        snsUserList.add(user);
        guardarFicheiroBinario(snsUserList);
    }

    public void addSNSUser(String name, String email, String pwd, String role) {
        AuthFacade user = new AuthFacade();
        user.addUserWithRole(name,email,pwd,role);
    }


    public String gerenatepwd() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 8;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString;
    }

    public boolean validateUser(String email) {
        AuthFacade user = new AuthFacade();
        if (user.existsUser(email))
            return true;
        else
            return false;
    }


    public boolean guardarFicheiroBinario (ArrayList<SNSUser> snsUserList) {
        return guardarFicheiroBinario(new File("C:\\Users\\marco\\OneDrive\\Desktop\\Serialization\\snsuserserialize.txt"),snsUserList);
    }

    public boolean guardarFicheiroBinario(File ficheiro, ArrayList<SNSUser> snsUserList) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(ficheiro));
            try { out.writeObject(snsUserList);
                return true;
            }finally {
                out.close();
            }
        }
        catch (IOException e) {
            return false;
        }
    }

}
