package app.domain.Store;

import app.domain.model.SNSUser;
import app.domain.shared.ScheduleVaccine;
import app.ui.console.ScheduleVaccineUI;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Collections;

public class    ScheduleVaccineStore extends ScheduleVaccineUI {
    final static ArrayList<ScheduleVaccine> VaccineAppointments = new ArrayList<>();
    final static ArrayList<String> Centro_Vacinaçao = new ArrayList<>();
    private ArrayUtils getSnsUserList;

    /**
     * Method to Instantiate WaitingRoomStore
     */
    public ScheduleVaccineStore() {

    }

    /**
     * Allows the system to change the typed vaccinationcenter
     * @param vaccinationcenter
     */
    public void setcenter(String vaccinationcenter) {
        Centro_Vacinaçao.add(vaccinationcenter);
    }


    /**
     * Gets by parameter SNSNumber and it compares with all SNSNumbers register in the schedule user array list
     * @param SnsUserNumber
     * @return true or false depending on validation (in)success
     */
    public boolean ValidateSnsUserNumber(ArrayList<SNSUser> SnsUserList, String SnsUserNumber) {
        for (int i = 0; i < SnsUserList.size(); i++) {
                if((SnsUserList.get(i).getSNSUserNumber()).equals(SnsUserNumber)){
                    return true;
                }
            }
        return false;
        }




    public ScheduleVaccineStore(String SnsUserNumber) {
        ScheduleVaccine sv = new ScheduleVaccine(SnsUserNumber);
    }

    public void setVaccineAppointments(String SnsUserNumber, String Center, String Date, String Time, String Vaccine){

    }


    public void addScheduleVaccine(String SnsUserNumber, String VaccinationCenter, String Date, String time, String Vaccine) {
        ScheduleVaccine sv = new ScheduleVaccine(SnsUserNumber, VaccinationCenter, Date, time, Vaccine);
        VaccineAppointments.add(sv);
    }
    public boolean validateScheduleVaccine (String SnsUserNumber, String VaccinationCenter, String Date, String time, String Vaccine) {
        return false;
    }

    /**
     * Gets by parameter scheduleVaccine and it compares with all scheduleVaccines register in the Vaccine Appointments list
     * @param scheduleVaccine
     * @return true or false depending on validation (in)success
     */
    public boolean checkDuplicatedVaccineApoointment(ScheduleVaccine scheduleVaccine){
        for (ScheduleVaccine scheduleVaccine1 : VaccineAppointments){
            if (scheduleVaccine1.equals(scheduleVaccine)){
                return true;
            }
        }
        return false;
    }
    public ArrayList<ScheduleVaccine> getVaccineAppointments() {
        return VaccineAppointments;
    }
}