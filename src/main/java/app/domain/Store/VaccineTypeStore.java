package app.domain.Store;

import app.domain.shared.Employee;
import app.domain.shared.VaccineType;
import app.ui.console.AddVaccineTypeUI;
import org.apache.commons.lang3.StringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.util.ArrayList;
import java.util.List;

public class VaccineTypeStore<Vt> extends AddVaccineTypeUI {
    ArrayList<VaccineType> VaccineTypes = new ArrayList<>();

    public VaccineTypeStore(String code, String designation, String whoId) {
        VaccineType vt = new VaccineType(code, designation, whoId);
    }

    public boolean validateVaccineType(String code) {
        if (code.length() == 5)
            return true;
        else
            return false;
    }
    public void saveVaccineType(String code, String designation, String whoId) {
        VaccineType vt = new VaccineType(code, designation, whoId);
        VaccineTypes.add(vt);
    }

    public void addVaccineType(String code, String designation, String whoId) {
        VaccineType vt = new VaccineType(code, designation, whoId);
        VaccineTypes.add(vt);
    }

    public boolean validateVaccineType(String code, String designation) {
        if (StringUtils.isBlank(code) || code.length() != 5 || StringUtils.isBlank(designation)) {
            return true;
        } else
            return false;
    }

}





