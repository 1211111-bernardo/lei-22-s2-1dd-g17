package app.domain.Store;

import app.domain.model.SNSUser;
import app.domain.shared.ArrivalUser;
import app.domain.shared.WaitingRoom;
import app.ui.console.WaitingRoomUI;
import dto.WaitingRoomDto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

public class WaitingRoomStore extends WaitingRoomUI {

    final static ArrayList<SNSUser> WaitingList= new ArrayList<>();

    final static ArrayList<String> Centro_Vacinaçao = new ArrayList<>();

    private String SaveSNSNumber;

    /**
     * Method to Instantiate WaitingRoomStore
     */
    public WaitingRoomStore(){

    }

    /**
     * Method to instantiate WaitingRoomStore
     * @param ARRIVAL_USER_ARRAY_LIST
     * @param snsUserArrayList
     */
    public WaitingRoomStore(ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST, ArrayList<SNSUser> snsUserArrayList){

    }

    /**
     *  gets by parameter all data required
     *  creates a new WaitingRoom
     * @param dto
     */
    public WaitingRoomStore (WaitingRoomDto dto) {
        ArrayList<SNSUser> snsUserList = dto.getSnsUserList();
        ArrayList<ArrivalUser> arrivalList=dto.getARRIVAL_USER_ARRAY_LIST();
        WaitingRoom room= new WaitingRoom(snsUserList,arrivalList);
    }

    /**
     * gets by parameter 2 arraylists required to execute this method
     * method responsible for creating a waitinglist
     * @param ARRIVAL_USER_ARRAY_LIST
     * @param snsUserArrayList
     * @return
     */
    public ArrayList<SNSUser> createWaitingList(ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST, ArrayList<SNSUser> snsUserArrayList){
        for (int i = 0; i < ARRIVAL_USER_ARRAY_LIST.size(); i++) {
            SaveSNSNumber=ARRIVAL_USER_ARRAY_LIST.get(i).getSNSNumber();
            for (int j = 0; j < snsUserArrayList.size(); j++) {
                if(SaveSNSNumber.equals(snsUserArrayList.get(j).getSNSUserNumber())){
                    WaitingList.add(snsUserArrayList.get(i));
                }
            }
        }
        guardarFicheiroBinario(WaitingList);
        return WaitingList ;
    }

    public boolean guardarFicheiroBinario (ArrayList<SNSUser> waitinglist) {
        return guardarFicheiroBinario(new File("C:\\Users\\marco\\OneDrive\\Desktop\\Serialization\\waitingroomserialization.txt"),waitinglist);
    }

    public boolean guardarFicheiroBinario(File ficheiro, ArrayList<SNSUser> waitingList) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(ficheiro));
            try { out.writeObject(waitingList);
                return true;
            }finally {
                out.close();
            }
        }
        catch (IOException e) {
            return false;
        }
    }

    /**
     * returns the waitinglist
     * @return waitinglist
     */

    public  ArrayList<SNSUser> getWaitingList(){
        return WaitingList;
    }

    /**
     * Allows the system to change the typed vaccinationcenter
     * @param vaccinationcenter
     */
    public void setcenter(String vaccinationcenter) {
        Centro_Vacinaçao.add(vaccinationcenter);
    }

    /**
     * Returns Centro_Vacinaçao
     * @return Centro_Vacinaçao
     */
    public String getcenter () {
        return Centro_Vacinaçao.get(0);
    }

    /**
     * Method responsible for showing/printing the waitinglist
     * @param WaitingList
     */
    public void printarray(ArrayList<SNSUser> WaitingList) {
        for (int i = 0; i < WaitingList.size(); i++) {
            System.out.printf("User in the Waiting Room:");
            System.out.printf("Name: %s \n", WaitingList.get(i).getName());
            System.out.printf("Email: %s \n", WaitingList.get(i).getEmail());
            System.out.printf("Birth Date: %s \n", WaitingList.get(i).getBirthDate());
            System.out.printf("Address: %s \n", WaitingList.get(i).getAdress());
            System.out.printf("SNS User Number: %s \n", WaitingList.get(i).getSNSUserNumber());
            System.out.printf("Citizen Card Number: %s \n", WaitingList.get(i).getCitizenCardNumber());
            System.out.println();
        }
    }
}
