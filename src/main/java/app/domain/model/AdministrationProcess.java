package app.domain.model;

import java.util.List;

/**
 * @author : Daniel Coutinho - 1201440
 **/
public class AdministrationProcess {

    private List<Integer> ageGroups;

    private List<Float> dosagePerAgeGroup;

    private List<Integer> dosesToBeAdministeredPerAgeGroup;

    private List<Integer> timeIntervalBetweenDoses;

    public AdministrationProcess(List<Float> dosages, List<Integer> ageGroup, List<Integer> dosesNumber, List<Integer> timeInterval) {
        try {
            setDosagePerAgeGroup(dosages);
            setAgeGroups(ageGroup);
            setDosesToBeAdministeredPerAgeGroup(dosesNumber);
            setTimeIntervalBetweenDoses(timeInterval);
        } catch (IllegalArgumentException e) {
            System.out.println("Error creating administration process due to: " + e);
        }
    }

    public List<Integer> getAgeGroups() {
        return ageGroups;
    }

    public void setAgeGroups(List<Integer> ageGroups) {
        this.ageGroups = ageGroups;
    }

    public List<Float> getDosagePerAgeGroup() {
        return dosagePerAgeGroup;
    }

    public void setDosagePerAgeGroup(List<Float> dosagePerAgeGroup) {
        this.dosagePerAgeGroup = dosagePerAgeGroup;
    }

    public List<Integer> getDosesToBeAdministeredPerAgeGroup() {
        return dosesToBeAdministeredPerAgeGroup;
    }

    public void setDosesToBeAdministeredPerAgeGroup(List<Integer> dosesToBeAdministeredPerAgeGroup) {
        this.dosesToBeAdministeredPerAgeGroup = dosesToBeAdministeredPerAgeGroup;
    }

    public List<Integer> getTimeIntervalBetweenDoses() {
        return timeIntervalBetweenDoses;
    }

    public void setTimeIntervalBetweenDoses(List<Integer> timeIntervalBetweenDoses) {
        this.timeIntervalBetweenDoses = timeIntervalBetweenDoses;
    }
}