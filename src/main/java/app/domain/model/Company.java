package app.domain.model;

import app.domain.Store.*;
import app.domain.shared.IDRole;
import app.domain.shared.VaccineType;
import dto.AUserDto;
import dto.AdministrateVaccineDTO;
import dto.ScheduleVaccineDTO;
import dto.WaitingRoomDto;
import pt.isep.lei.esoft.auth.AuthFacade;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company {

    private String designation;
    private AuthFacade authFacade;
    private SNSUserSto SNSUserSto;
    private ScheduleVaccineStore ScheduleVaccineStore;
    private AdministrateVaccineStore AdministrateVaccineStore;
    public Company(String designation)
    {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");

        this.designation = designation;
        this.authFacade = new AuthFacade();
        this.SNSUserSto = new SNSUserSto();
    }

    public Company(WaitingRoomDto dto) {
    }

    /**
     * Gets by parameter all data required to register a new arrival user
     * @param dto has all required data (SNS Number,Time)
     */
    public Company(AUserDto dto) {
    }


    public Company(ScheduleVaccineDTO dto) {
    }
    /**
     * Gets by parameter all data required to register a new administration of a vaccine
     * @param dto has all required data
     */
    public Company(AdministrateVaccineDTO dto) {
    }

    /**
     * Gets by parameter all data required to register a new arrival user
     * Creates new ArrivalStore with the same data
     * @param dto has all required date (SNS Number, Time)
     */
    public void createArrivalUser (AUserDto dto) {
        ArrivalStore Auser = new ArrivalStore(dto);
    }

    public void createWaitingList (WaitingRoomDto dto) {
        WaitingRoomStore room = new WaitingRoomStore(dto);
    }

    public SNSUserSto getSNSUserSto(){
        return SNSUserSto;
    }

    public String getDesignation() {
        return designation;
    }

    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    public AdministrateVaccineStore getAdministrateVaccineStore() {
        return AdministrateVaccineStore;
    }

    public ScheduleVaccineStore getScheduleVaccineStore() {return ScheduleVaccineStore;}

    public ArrayList<String> getIdRoles() {
        IDRole idroles=new IDRole();
        return idroles.getIdRoles();
    }
    private List<VaccineType> vaccineTypeList;
    public VaccineType createVaccineType (String code, String designation, String whoId) {
        return new VaccineType(code, designation, whoId);
    }
    public boolean validateVaccineType (VaccineType vt) {
        if (vt == null)
            return false;
        return ! this.vaccineTypeList.contains(vt);
    }
   /* public boolean saveVaccineType (VaccineType vyt) {
        VaccineType vt;
        if (validateVaccineType(vt)) {
            return this.vaccineTypeList.add(vt);
        }
        return false;
    }*/
}