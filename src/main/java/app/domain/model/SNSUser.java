package app.domain.model;

public class SNSUser {

   private String name;
   private String adress;
   private String email;
   private String PhoneNumber;
   private String Sex;
   private String BirthDate;
   private String SNSUserNumber;
   private String CitizenCardNumber;

    public SNSUser(String name, String adress, String email, String phoneNumber, String birthDate, String SNSUserNumber, String citizenCardNumber, String Sex) {
        this.name = name;
        this.adress = adress;
        this.email = email;
        PhoneNumber = phoneNumber;
        BirthDate = birthDate;
        this.SNSUserNumber = SNSUserNumber;
        CitizenCardNumber = citizenCardNumber;
        this.Sex=Sex;
    }


    public SNSUser(String name, String adress, String email, String phoneNumber, String birthDate, String SNSUserNumber, String citizenCardNumber) {
        this.name = name;
        this.adress = adress;
        this.email = email;
        PhoneNumber = phoneNumber;
        BirthDate = birthDate;
        this.SNSUserNumber = SNSUserNumber;
        CitizenCardNumber = citizenCardNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public String getSNSUserNumber() {
        return SNSUserNumber;
    }

    public void setSNSUserNumber(String SNSUserNumber) {
        this.SNSUserNumber = SNSUserNumber;
    }

    public String getCitizenCardNumber() {
        return CitizenCardNumber;
    }

    public void setCitizenCardNumber(String citizenCardNumber) {
        CitizenCardNumber = citizenCardNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

