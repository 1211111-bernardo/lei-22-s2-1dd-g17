package app.domain.model;

import java.util.List;

/**
 * @author : Daniel Coutinho - 1201440
 **/
/*public class Vaccine {

    /**
     * Brand of the vaccine (for instance: Pfizer, moderna, etc...)
     */
    /*private String brand;

    /**
     * Administration process associated to a specific vaccine (age groups, dosage for each age group, number of doses for each age group and time interval between doses).
     */
   /* private AdministrationProcess administrationProcess;

    /**
     * The type of vaccine that this vaccine is going to work (vaccine type, like COVID-19, Tetanus, etc...)
     */
    /*private VaccineType vaccineType;

    /**
     * Creates an instance of Vaccine.
     *
     * @param brand  brand of the vaccine, for instance Pfizer for COVID-19 and Afluria Quadrivalent for flu.
     * @param addPro administration process associated with a specific vaccine.
     * @param vacTy  type of vaccine that this vaccine is going to work, for instance COVID-19, diphtheria, etc...
     */
   /* public Vaccine(String brand, AdministrationProcess addPro, VaccineType vacTy) {
        try {
            setAddPro(addPro);
            setVacTy(vacTy);
            setBrand(brand);
        } catch (IllegalArgumentException e) {
            System.out.println("Error creating vaccine due to: " + e);
        }
    }

    /**
     * Shows the administration process associated to the actual vaccine.
     *
     * @return the administration process associated to the actual vaccine.
     */
    /*public AdministrationProcess getAddPro() {
        return administrationProcess;
    }
/*
    /**
     * Sets the administration process of the implicit vaccine.
     *
     * @param addPro changes the old administration process to this, passed by argument.
     */
    /*public void setAddPro(AdministrationProcess addPro) {
        this.administrationProcess = addProp;
    }

    /**
     * Shows the vaccine type of the implicit vaccine.
     *
     * @return the vaccine type of the implicit vaccine.
     */
   /* public VaccineType getVacTy() {
        return vaccineType;
    }

    /**
     * Sets the vaccine type of the implicit vaccine.
     *
     * @param vacTy changes the old vaccine type to this, passed by argument.
     */
    /*public void setVacTy(VaccineType vacTy) {
        this.vaccineType = vacTy;
    }

    /**
     * Shows the brand of the implicit vaccine.
     *
     * @return the brand of the implicit vaccine.
     */
    /*public String getBrand() {
        return brand;
    }

    /**
     * Sets the brand of the implicit vaccine.
     *
     * @param brand changes the old brand to this, passed by argument.
     */
    /*public void setBrand(String brand) {
        if (brand.length() == 0 || brand.equals(null))
            throw new IllegalArgumentException();
        this.brand = brand;
    }

    /**
     * Creates an administration process.
     */
    /*private List<AdministrationProcess> administraionProcessList;

   /* public Administrationprocess createcreateAdministrationProcess(List<Float> dosages, List<Integer> ageGroup, List<Integer> dosesNumber, List<Integer> timeInterval) {
        return new AdministrationProcess(List < Float > dosages, List < Integer > ageGroup, List < Integer > dosesNumber, List < Integer > timeInterval);
    }

    /**
     * Validates a vaccination process.
     *
     * @param addPro is the administration process to validate.
     * @return true if it is valid, or false if is not valid.
     */
    /*private boolean validateAdministrationProcess(AdministrationProcess addPro) {
        if (addPro == null)
            return false;
        return !this.administraionProcessList.contains(addPro);
    }


    /**
     * Saves an administration process.
     * @param addPro is the administration process to save.
     * @return true if it was successfully saved, or false if it wasn't.
     */
    /*public boolean saveAdministrationProcess(AdministrationProcess addPro){
        if(!validateAdministrationProcess(addPro))
            return false;
        return this.administraionProcessList.add(addPro);
    }

    /**
     * Adds an administration process.
     *
     * @param addPro is the administration process to add.
     */
    /*private void addAdministrationProcess(AdministrationProcess addPro){
        administraionProcessList.add(addPro);
    }
   /* public void showInfo() {
        System.out.println("\t| Vaccine " + this.getBrand() + " |");
        System.out.println("\nVaccine Type: " + this.vaccineType.getName());
        System.out.println("\nAge groups and respective dosage");
        int k = 0;

        for (int i = 0; i < this.administrationProcess.getDosagePerAgeGroup().size(); i++) {
            System.out.println((i + 1) + "º [" + this.administrationProcess.getAgeGroups().get(k) + ", " + this.administrationProcess.getAgeGroups().get(k + 1) + "] -> " + this.administrationProcess.getDosagePerAgeGroup().get(i) + "mL");
            k += 2;
        }
        k = 0;
        System.out.println("\nNumber of doses per age group");
        for (int i = 0; i < this.administrationProcess.getDosesToBeAdministeredPerAgeGroup().size(); i++) {
            System.out.println((i + 1) + "º [" + this.administrationProcess.getAgeGroups().get(k) + ", " + this.administrationProcess.getAgeGroups().get(k + 1) + "] -> " + this.administrationProcess.getDosesToBeAdministeredPerAgeGroup().get(i) + " doses");
            k += 2;
        }
        k = 0;
        System.out.println("\nTime interval between doses");
        int z = 0;
        for (int i = 0; i < this.administrationProcess.getDosesToBeAdministeredPerAgeGroup().size(); i++) {
            System.out.println((i + 1) + "º [" + this.administrationProcess.getAgeGroups().get(k) + ", " + this.administrationProcess.getAgeGroups().get(k + 1) + "]");
            k += 2;
            for (int j = 0; j < this.administrationProcess.getDosesToBeAdministeredPerAgeGroup().get(i) - 1; j++) {
                System.out.println(this.administrationProcess.getTimeIntervalBetweenDoses().get(z++) + " days from " + (j + 1) + "º to " + (j + 2) + "º");
            }
        }
    }
}*/
