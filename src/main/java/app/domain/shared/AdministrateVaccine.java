package app.domain.shared;

import app.domain.model.SNSUser;

import java.util.ArrayList;

public class AdministrateVaccine {

    /**
     * The SNS User List that contains all user information
     */
    private ArrayList<SNSUser> snsUserList= new ArrayList<>();

    /**
     * All attributes needed to instantiate a new vaccine administration
     */
    private String snsUserNumber;
    private String vaccine;
    private String dose;
    private String type;


    /**
     * Creates an instance of AdministrateVaccine.
     * @param snsUserNumber
     * @param vaccine
     * @param dose
     * @param type

     */
    public AdministrateVaccine(String snsUserNumber, String type, String vaccine, String dose) {
        this.snsUserNumber = snsUserNumber;
        this.type = type;
        this.vaccine = vaccine;
        this.dose = dose;
    }

    /**
     * This method is needed in the UI to create a new AdministrateVaccine Object
     */
    public AdministrateVaccine() {

    }
}
