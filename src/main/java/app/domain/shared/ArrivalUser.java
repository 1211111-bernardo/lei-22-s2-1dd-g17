package app.domain.shared;

import java.io.Serializable;

public class ArrivalUser implements Serializable {
    /**
     * All attributes needed to instantiate a new employee
     */
    private String SNSNumber;
    private String Time;
    /**
     * Creates an instance of ArrivalUser.
     * @param SNSNumber
     * @param Time

     */
    public ArrivalUser (String SNSNumber,String Time) {
        this.SNSNumber = SNSNumber;
        this.Time = Time;
    }
    /**
     * Shows the Number of the SNS User.
     *
     * @return the name of the implicit SNS User.
     */
    public String getSNSNumber() {
        return SNSNumber;
    }
    /**
     * Shows the Time that the SNS User arrived.
     *
     * @return the time that the SNS User arrived.
     */
    public String getTime() {
        return Time;
    }

    /**
     * Shows all information
     * @return String with all data organized
     */
    public String ToString () {
        return String.format("SNS User arrived:\nSNS Number: %s \nArrived Time: %s",SNSNumber,Time);
    }
}
