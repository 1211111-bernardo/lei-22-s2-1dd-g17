package app.domain.shared;

public class Center {
    private String name;
    private String adress;
    private String phone_number;
    private String email;
    private String fax_number;
    private String website_adress;
    private String slot_duration;
    private String max_vaccines_per_slot;
    private String coordinator;

    public Center (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
