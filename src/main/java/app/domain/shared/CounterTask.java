package app.domain.shared;

import app.domain.Store.AdministrateVaccineStore;
import app.domain.Store.CenterStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimerTask;

public class CounterTask extends TimerTask {
    @Override
    public void run() {
/**
 * Gets AdministrateVaccineStore to have access to their methods
 */
        AdministrateVaccineStore store= new AdministrateVaccineStore();

        System.out.println("Counter Task being Executed...");
        /**
         * gets the counter of the daily number of people vaccinated
         */
        int size= store.CounterArrayAdministrate();
        System.out.printf("Number of people vaccinated today: %d  \n", size);
/**
 * Line of code responsible for getting the LocalDateTime at the moment executed
 */
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println("\n Date of execution: ");
        System.out.printf(dtf.format(now), "\n");

        try {
            WriteCSVFile(now,size);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }


    /**
     * Responsible for writing the data and the counter of daily people vaccinated to a CSVFile
     * @param date
     * @param size
     * @throws FileNotFoundException
     */
    public void WriteCSVFile(LocalDateTime date, int size) throws FileNotFoundException {
        File CSVFile= new File("Counter.csv");
        PrintWriter out= new PrintWriter(CSVFile);
        out.print(date);
        out.print(";");
        out.print(size);
        out.println();
        out.close();
    }
}
