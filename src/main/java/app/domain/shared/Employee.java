package app.domain.shared;

import java.io.Serializable;

/**
 * @author : Bernardo Azevedo - 1211111
 **/

public class Employee implements Serializable {
    /**
     * All attributes needed to instantiate a new employee
     */
    private String name;
    private String pwd;
    private String email;
    private String IDRole;

    /**
     * Creates an instance of Employee.
     * @param name
     * @param pwd
     * @param email
     * @param IDRole
     */
    public Employee(String name, String pwd, String email, String IDRole) {
        this.name = name;
        this.pwd = pwd;
        this.email = email;
        this.IDRole = IDRole;
    }

    /**
     * Shows the name of the employee.
     *
     * @return the name of the implicit employee.
     */
    public String getName() {
        return name;
    }
    /**
     * Shows the password of the employee.
     *
     * @return the pwd of the implicit employee.
     */
    public String getPwd() {
        return pwd;
    }
    /**
     * Shows the email of the employee.
     *
     * @return the email of the implicit employee.
     */
    public String getEmail() {
        return email;
    }
    /**
     * Shows the role of the employee.
     *
     * @return the IDRole of the implicit employee.
     */
    public String getIDRole() {
        return IDRole;
    }
    /**
     * Sets the name of the implicit employee.
     *
     * @param name changes the old name to this, passed by argument.
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * Sets the password of the implicit employee.
     *
     * @param pwd changes the old password to this, passed by argument.
     */
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    /**
     * Sets the email of the implicit employee.
     *
     * @param email changes the old email to this, passed by argument.
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * Sets the role of the implicit employee.
     *
     * @param IDRole changes the old role to this, passed by argument.
     */
    public void setIDRole(String IDRole) {
        this.IDRole = IDRole;
    }

    /**
     * Shows all Employee attributes.
     * @return text containing all employee attributes.
     */
    public String toString() {
        return String.format("Registed Employee: \n Name: %s \n Email: %s \n Role: %s \n Generated password: %s", name, email, IDRole, pwd);
    }
}

