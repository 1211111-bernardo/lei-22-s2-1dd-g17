package app.domain.shared;

import java.util.ArrayList;
import java.util.List;

public class IDRole {
    private String R1 = "Nurse";
    private String R2 = "Coordinator";
    private String R3 = "Receptionist";
    public ArrayList<String> getIdRoles() {
        ArrayList<String> IdRoles = new ArrayList<String>();
        IdRoles.add(R1);
        IdRoles.add(R2);
        IdRoles.add(R3);
        return IdRoles;
    }
    // List containing all Employee Roles available
}
