package app.domain.shared;

import app.controller.App;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ImportFiles {public boolean saveUsersData(String file) throws FileNotFoundException {
    File f = new File(file);
    String spliter = findSpliter(file);
    if (!f.exists()) {
        throw new FileNotFoundException("There is no file with such path!");
    }

    try (Scanner sc = new Scanner(f)) {
        while (sc.hasNext()) {
            try {
                String[] line = sc.nextLine().split(spliter);
                String name = line[0];
                String sex = line[1];
                String birthday= line[2];
                String adress = line[3];
                String phoneNumber = line[4];
                String email = line[5];
                String snsNumber = line[6];
                String ccNumber = line[7];
                String password = App.getInstance().getCompany().getSNSUserSto().gerenatepwd();
                App.getInstance().getCompany().getSNSUserSto().saveSNSUser(name,email,adress,phoneNumber,birthday,snsNumber,ccNumber, sex);
                App.getInstance().getCompany().getSNSUserSto().addSNSUser(name,email,password,Constants.ROLE_SNS_USER);
            } catch (NumberFormatException e) {
                continue;
            }

        }
    }
    return true;
}

    private String findSpliter(String file) throws FileNotFoundException {
        File f = new File(file);
        Scanner sc = new Scanner(f);
        String s = sc.nextLine();
        if (s.matches(".*;.*")){
            return ";";
        }
        return ",";
    }
}
