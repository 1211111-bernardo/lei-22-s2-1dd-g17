package app.domain.shared;

public class PerformanceCenter {
    private String Date;
    private String m;
    private String ArrivalTime;
    private String LeavingTime;
    /**
     * Constructor that allows to instantiate a new PerformanceDTO
     * @param Date corresponds to the date that the center will be analyzed
     * @param TimeInterval corresponds to the time interval of the analysis
     */
    public PerformanceCenter (String Date, String TimeInterval) {
        this.Date = Date;
        this.m = TimeInterval;
    }
    /**
     * Constructor that allows to instantiate a new PerformanceDTO
     * @param ArrivalTime corresponds to the time that the user arrival the center
     * @param LeavingTime corresponds to the time that the user leave the center
     * @param m corresponds to the time interval of the analysis
     */
    public PerformanceCenter (String ArrivalTime,String LeavingTime,String m){
        this.ArrivalTime=ArrivalTime;
        this.LeavingTime = LeavingTime;
        this.m = m;
    }
    /**
     * Shows the date of analyze.
     *
     * @return the Date.
     */
    public String getDate() {
        return Date;
    }
    /**
     * Shows the Time Interval of analyze.
     *
     * @return the Time Interval.
     */
    public String getM() {
        return m;
    }

    public String getArrivalTime() {
        return ArrivalTime;
    }

    public String getLeavingTime() {
        return LeavingTime;
    }
    /**
     * Shows all information
     * @return String with all data organized
     */
    public String ToString() {
        return String.format("Performance analysis:\n Day: %s \n Time Interval: %s",Date,m);
    }
}
