package app.domain.shared;

import app.domain.Store.AdministrateVaccineStore;

import java.util.TimerTask;

public class RemoveTask extends TimerTask {

    public void run(){
        System.out.println("\n\n Removing all the users from the list...");
        AdministrateVaccineStore store= new AdministrateVaccineStore();
        store.removeUserFromCentre();
        System.out.println("Users removed with great success!\n");
    }
}
