package app.domain.shared;

public class ScheduleVaccine {
    private String SnsUserNumber;
    private String Center;
    private String Date;
    private String Time;
    private String Type;

    /**
     * Creates an instance of ScheduleVaccine
     * @param SnsUserNumber
     * @param Time
     * @param Center
     * @param Date
     * @param Type
     */
    public ScheduleVaccine(String SnsUserNumber, String Center, String Date, String Time, String Type) {
        this.SnsUserNumber = SnsUserNumber;
        this.Center = Center;
        this.Date = Date;
        this.Time = Time;
        this.Type = Type;
    }

    /**
     * Creates an instance of ScheduleVaccine.
     * @param SnsUserNumber
     */
    public ScheduleVaccine(String SnsUserNumber){
        this.SnsUserNumber = SnsUserNumber;
    }


    /**
     * Shows all information
     * @return String with all data organized
     */
    public String toString() {
        return String.format("Registed Vaccine Appointment: \n SnsUserNumber: %s \n Vaccination Center: %s \n Date: %s \n Time: %s \n Type: %s", SnsUserNumber, Center, Date, Time, Type);
    }

    public String getSnsUserNumber() {
        return SnsUserNumber;
    }
}
