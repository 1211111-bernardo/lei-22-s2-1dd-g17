package app.domain.shared;


import java.text.ParseException;

import java.util.Calendar;
import java.util.Timer;

import java.util.concurrent.TimeUnit;

public class TimerDemo extends Thread{


    public  void run()  {
        try {
            /**
             * Creating a fixed hour to execute the Counter Task (number of vaccinated people)
             * Using the method "set" we can set the hour we want to execute it
             */
            Calendar today = Calendar.getInstance();
            today.set(Calendar.HOUR_OF_DAY, 20);
            today.set(Calendar.MINUTE, 30);
            today.set(Calendar.SECOND, 0);

            /**
             * Creating a fixed hour to execute the Remove Task( everyday it removes the vaccinated people from the centres)
             * Using the method "set" we can set the hour we want to execute it
             */

            Calendar everyday= Calendar.getInstance();
            everyday.set(Calendar.HOUR_OF_DAY, 21);
            everyday.set(Calendar.MINUTE, 30);
            everyday.set(Calendar.SECOND, 0);


// every day at 8pm you run your CounterTask and at 9.30pm you run your RemoveTask (removing vaccinated users from the center)
            /**
             * instantiates a new Timer to schedule the tasks we want
             * no parameters needed
             */

            Timer timer = new Timer();

            /**
             * schedules 2 tasks by instantiating them, at a fixed rate of 1 day (executes the tasks every day)
             * timer.Schedule uses by parameters: Name of the task, time of execution, delay of the execution, and the interval of execution(1 day)
             */

            timer.schedule(new CounterTask(), today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)); // period: 1 day
            timer.schedule(new RemoveTask(), everyday.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)); // period: 1 day
            System.out.println("Timer Demo has scheduled the Tasks...");
        }catch (RuntimeException exception){
            System.out.println("Error Occurred during the execution of the task!");
        }
    }

}