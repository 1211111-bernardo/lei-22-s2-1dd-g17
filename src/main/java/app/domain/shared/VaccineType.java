package app.domain.shared;

import org.apache.commons.lang3.StringUtils;

public class VaccineType {
    private String code;
    private String designation;
    private String whoId;

    public VaccineType(String code, String designation, String whoId) {
        this.code = code;
        this.designation = designation;
        this.whoId = whoId;
    }



    public String toString() {
        return String.format("Registed Vaccine Type: \n Code: %s \n Designation: %s \n ID: %s ", code, designation, whoId);
    }


}


