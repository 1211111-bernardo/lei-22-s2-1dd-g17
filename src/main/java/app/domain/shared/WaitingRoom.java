package app.domain.shared;

import app.domain.model.SNSUser;

import java.util.ArrayList;

public class WaitingRoom {



    private ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST = new ArrayList<>();

    private ArrayList<SNSUser> snsUserList= new ArrayList<>();

    /**
     * Constructor that gets 2 arraylists by parameter to instantiate WaitingRoom
     * @param snsUserList
     * @param arrivalList
     */
    public WaitingRoom(ArrayList<SNSUser> snsUserList, ArrayList<ArrivalUser> arrivalList){
        this.ARRIVAL_USER_ARRAY_LIST=ARRIVAL_USER_ARRAY_LIST;
        this.snsUserList= this.snsUserList;
    }

    /**
     * Returns arrivaluserarraylist
     * @return arrivaluserarraylist
     */
    public ArrayList<ArrivalUser> getARRIVAL_USER_ARRAY_LIST() {
        return ARRIVAL_USER_ARRAY_LIST;
    }

    public void setARRIVAL_USER_ARRAY_LIST(ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST) {
        this.ARRIVAL_USER_ARRAY_LIST = ARRIVAL_USER_ARRAY_LIST;
    }

    /**
     * Returns snsuserlist
     * @return snsuserlist
     */
    public ArrayList<SNSUser> getSnsUserList() {
        return snsUserList;
    }

    public void setSnsUserList(ArrayList<SNSUser> snsUserList) {
        this.snsUserList = snsUserList;
    }


}
