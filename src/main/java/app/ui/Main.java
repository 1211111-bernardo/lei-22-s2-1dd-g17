package app.ui;

import app.domain.shared.TimerDemo;
import app.ui.console.MainMenuUI;


import java.sql.Time;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

//Teste
public class Main {

    public static void main(String[] args)
    {
        TimerDemo thread1= new TimerDemo();
        thread1.start();
        try
        {
            MainMenuUI menu = new MainMenuUI();
            menu.run();

        }
        catch( Exception e )
        {
            e.printStackTrace();
        }

    }

}
