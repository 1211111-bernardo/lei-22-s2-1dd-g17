package app.ui.console;

import app.controller.ArrivalController;
import app.controller.CenterController;
import app.ui.console.utils.Utils;

public class AddCenterUI implements Runnable{
    private CenterController centerctrl;
    public AddCenterUI() {
        centerctrl = new CenterController();}
    public void run() {
        System.out.printf("Register new vaccination center:");
        String name = Utils.readLineFromConsole("Enter Center Name: ");
        System.out.println(centerctrl.createCenter(name));
    }

}
