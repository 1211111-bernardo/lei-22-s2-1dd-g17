package app.ui.console;

import app.controller.AddUserController;
import app.domain.shared.Employee;
import app.domain.Store.EmployeeStore;
import app.ui.console.utils.Utils;

public class AddUserUI implements Runnable {
    private AddUserController AUctrl;
   public AddUserUI() {
       AUctrl = new AddUserController();
   }
     public void run() {
         System.out.printf ("Register new Employee:");
         String name = Utils.readLineFromConsole("Enter Employee name: ");
         String email = Utils.readLineFromConsole("Enter Employee email: ");
         String address = Utils.readLineFromConsole("Enter Employee address: ");
         String Pnumber = Utils.readLineFromConsole("Enter Employee Phone Number: ");
         String CCN = Utils.readLineFromConsole("Enter Employee Citizen Card Number: ");
         String pwd = AUctrl.generatepwd(name,email,null,null);
         System.out.println("");
         System.out.println("Choose the Employee Role:");
       int i = 0;
         for (String number : AUctrl.getIdRoles()) {
             i++;
             System.out.printf("%d- %s",i,number);
             if (i < 3)
                 System.out.println("");
         }
         String role = Utils.readLineFromConsole("");
         if (role.equals("1")) {
             role = "Nurse";
         } else if(role.equals("2")) {
             role = "Coordinator";
         } else {
             role = "Receptionist";
         }
        AUctrl.addUserWithRole(name,email,pwd,role);
         Employee Emp = new Employee(name,pwd,email,role);
         System.out.println(Emp.toString());
         System.out.printf(" Address: %s\n",address);
         System.out.printf(" Card Number: %s\n",CCN);
         System.out.printf(" Phone Number: %s\n",Pnumber);
        String confirmation = Utils.readLineFromConsole("Do you confirm the data? ");
         if (confirmation.equals("yes")) {
             EmployeeStore emp = new EmployeeStore(name, pwd, email, role);


             if (emp.validateEmployee(email)) {
                 System.out.println("Invalid Registration");

             } else {
                 emp.addEmployee(name, pwd, email, role);
             emp.saveEmployee(name,pwd,email,role);
             System.out.println("Employee added");}
         }

    }

}
