package app.ui.console;

import app.controller.SpecifyNewVaccineTypeController;
import app.domain.Store.VaccineTypeStore;
import app.domain.shared.VaccineType;
import app.ui.console.utils.Utils;

public class AddVaccineTypeUI implements Runnable {
    private SpecifyNewVaccineTypeController AUctrl;

    public AddVaccineTypeUI() {
        AUctrl = new SpecifyNewVaccineTypeController();
    }

    public void run() {
        System.out.printf("Register new VaccineType:");
        String code = Utils.readLineFromConsole("Enter Vaccine Code: ");
        String designation = Utils.readLineFromConsole("Enter Vaccine Designation: ");
        String whoId = Utils.readLineFromConsole("Enter Vaccine Technology: ");
        VaccineTypeStore vtstore = new VaccineTypeStore(code, designation, whoId);
            vtstore.addVaccineType(code, designation, whoId);
            System.out.printf(" Code: %s\n", code);
            System.out.printf(" Designation: %s\n", designation);
            System.out.printf(" Vaccine Technology: %s\n", whoId);
            String confirmation = Utils.readLineFromConsole("Do you confirm the data? ");
            if (confirmation.equals("yes")) {
                if (vtstore.validateVaccineType(code, designation)) {
                    System.out.println("Invalid Registration");
                }
                else{
                    vtstore.addVaccineType(code, designation, whoId);
                    VaccineType vt = new VaccineType(code, designation, whoId);
                System.out.println("VaccineType added");
            }
        }
    }
}