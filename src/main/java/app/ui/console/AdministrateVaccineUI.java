package app.ui.console;

import app.controller.AdministrateVaccineController;
import app.domain.Store.AdministrateVaccineStore;
import app.domain.Store.SNSUserSto;
import app.domain.model.SNSUser;
import app.domain.shared.AdministrateVaccine;
import app.ui.console.utils.Utils;
import dto.AdministrateVaccineDTO;



import java.util.Scanner;
import java.util.ArrayList;

public class AdministrateVaccineUI implements Runnable{
    /**
     * All attributes needed to instantiate a new vaccine administration
     */
    private final AdministrateVaccineController AVcontroller;
    public AdministrateVaccineUI() {
        AVcontroller = new AdministrateVaccineController();
    }
    int SNSnum;

    @Override
    public void run() {
        AdministrateVaccineStore avstore = new AdministrateVaccineStore();
        AdministrateVaccineDTO dto;
        AdministrateVaccine AdministrateVaccine = new AdministrateVaccine();
        SNSUserSto SUSs = new SNSUserSto();
        System.out.println("Registed SNS Users:");
        ArrayList<SNSUser> snsUserList = SUSs.getSnsUserList();
        /**
         * This for is responsible to go through all the snsUserList and print all Sns Users Numbers registed
         */
        for(int i = 0; i < snsUserList.size(); i++) {
            System.out.print(+i+ " - ");
            System.out.println(snsUserList.get(i).getSNSUserNumber());
        }
        System.out.println("Select the SNS User Number wanted: ");
        Scanner j = new Scanner(System.in);
        SNSnum = j.nextInt();
        System.out.println("Name of the SNS User:");
        System.out.println(snsUserList.get(SNSnum).getName());
        System.out.println("Age of the SNS User:");
        AVcontroller.AgeCalculator(SNSnum);
        System.out.println("No Adverse Reactions registered in the system");
        String Type = Utils.readLineFromConsole("Write the Vaccine Type: ");
        String Vaccine = Utils.readLineFromConsole("Write the Vaccine: ");
        String Dose = Utils.readLineFromConsole("Write the Dose: ");
        dto = new AdministrateVaccineDTO(snsUserList.get(SNSnum).getSNSUserNumber(), Type, Vaccine, Dose);
        avstore.addAdministrateVaccine(snsUserList.get(SNSnum).getSNSUserNumber(), Type, Vaccine, Dose);
    }
}
