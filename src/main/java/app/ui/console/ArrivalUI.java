package app.ui.console;

import app.controller.AddUserController;
import app.controller.ArrivalController;
import app.domain.Store.ArrivalStore;
import app.domain.Store.EmployeeStore;
import app.domain.shared.ArrivalUser;
import app.domain.shared.Employee;
import app.ui.console.utils.Utils;
import dto.AUserDto;

public class ArrivalUI implements Runnable{
    private ArrivalController AUserctrl;
    public ArrivalUI() {
        AUserctrl = new ArrivalController();}
    int i = 0;
    public void run() {
        int flag = 0;
        System.out.printf("Register the arrival of a SNS User:");
        AUserDto dto;
        /**
         * The programme only request the vaccination center once per run
         * All ArrivalUsers registers are associated with the respective center
         * If i is different than 1 the programme will not request the vaccination center avoiding repetition in terms of requested data
         */
        i = i + 1;
        if (i == 1 ) {
            String VaccinationCenter = Utils.readLineFromConsole("Enter the Vaccination Center Name: ");
        AUserctrl.setVaccinationCenter(VaccinationCenter);}
        /**
         * In this section all data are being requested
         * The programme only stops this cicle when the user confirms the typed data
         */
        do {
            String SNSNumber = Utils.readLineFromConsole("Enter User Number: ");
            String Time = AUserctrl.getTime();
            dto = new AUserDto(SNSNumber, Time);
            AUserctrl.createArrivalUser(dto);
            ArrivalUser AUser = new ArrivalUser(dto.getSNSNumber(), dto.getTime());
            System.out.println(AUser.ToString());
            String Confirmation = Utils.readLineFromConsole("Do you want to confirm?");
            if (Confirmation.equals("yes"))
                flag = 1;
        } while (flag == 0);
        System.out.println(AUserctrl.saveArrival(dto.getSNSNumber(),dto.getTime()));
       // String print = Utils.readLineFromConsole("Do you want to print?");
        /**
         * !!! THE FOLLOWING 2 METHODS ARE NOT PERMANENT !!!
         * !!! They are used to help the group check if the data are being registed in a correct way !!!
         */
       /* if (print.equals("yes")) {
            AUserctrl.tostring();
            System.out.println(AUserctrl.getVaccinationCenter());
        }

        */

    }
}
