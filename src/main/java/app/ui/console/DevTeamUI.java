package app.ui.console;

import java.awt.*;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable{

    public DevTeamUI()
    {
    }
    public void run()
    {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t Bernardo Azevedo - 1211111@isep.ipp.pt \n");
        System.out.printf("\t Marco Moreira - 1211119@isep.ipp.pt \n");
        System.out.printf("\t Gabriel Sousa - 1211115@isep.ipp.pt \n");
        System.out.printf("\t Nuno Tavares - 1211024@isep.ipp.pt \n");
        System.out.printf("\t Daniel Coutinho - 1201440@isep.ipp.pt \n");
        System.out.printf("\t Jorge Sousa - 1210820@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
