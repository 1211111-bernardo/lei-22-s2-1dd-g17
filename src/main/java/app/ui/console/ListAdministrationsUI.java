package app.ui.console;

import app.ui.console.utils.Utils;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

import static javafx.application.Application.launch;

public class ListAdministrationsUI extends Application implements Runnable {
    public ListAdministrationsUI(){

    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(ListAdministrationsUI.class.getResource("listAdministrations.fxml"));
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("listAdministrations.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setScene(scene);
        stage.show();
    }


    @Override
    public void run() {
        launch();
    }
}
