package app.ui.console;

import app.controller.ArrivalController;
import app.domain.Store.LoginStore;
import app.ui.console.utils.Utils;

public class LoginUI {
    private String Role;
    public String getRole() {
        return Role;
    }
    public void setRole(String role) {
        Role = role;
    }
    private LoginStore loginStore;
    public LoginUI() {
        loginStore = new LoginStore();}

    public int log() {
        int flag = 0;
        System.out.println("Login:");
        String admin = Utils.readLineFromConsole("Do you want to run the app as a Administrator?");
        if (admin.equals("yes")) {
            String adminpwd = Utils.readLineFromConsole("Type the Admin password:");
            if (adminpwd.equals("1234")) {
                setRole("Admin");
                System.out.println("Admin logged with success!");
                return flag = 1;
            } else {
                System.out.println("The Admin password is wrong!");
                return flag;
            }
        } else {
            String email = Utils.readLineFromConsole("Type the User email:");
            String pwd = Utils.readLineFromConsole("Type the password:");
            if (loginStore.ValidateUser(email, pwd)) {
                if(loginStore.getRole(email).equals("Nurse")){
                    setRole("Nurse");
                    return flag = 1;
                } else if(loginStore.getRole(email).equals("Coordinator")){
                    setRole("Coordinator");
                    return flag = 1;
                }else if(loginStore.getRole(email).equals("Receptionist")){
                    setRole("Receptionist");
                    return flag = 1;
                }
            } else{
                System.out.println("Invalid User!");
                return flag ;}
        }
        return flag;
    }
}
