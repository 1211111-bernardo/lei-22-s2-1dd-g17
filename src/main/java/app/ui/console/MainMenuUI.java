package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class MainMenuUI {
    static List<MenuItem> options = new ArrayList<MenuItem>();
    static int flag = 0;
    Scanner ler = new Scanner(System.in);
    public MainMenuUI() {}
    public void run() {
        int intlog = 0;
        LoginUI login = new LoginUI();
        flag ++;
        do {
            intlog = login.log();
        } while (intlog == 0);
        if (intlog == 1) {
            System.out.printf("Corrent User logged: ");
            System.out.println(login.getRole());
            switch (login.getRole()) {
                case ("Receptionist"):
                    int option1 = -1;
                    do {
                        System.out.println("1 - Logout");
                        System.out.println("6 - Register new SNS User");
                        System.out.println("7 - Register the arrival of a SNS User");
                        System.out.println("9 - Schedule a Vaccine");
                        System.out.println("0 - Exit");
                        System.out.println("Type your option:");
                        option1 = ler.nextInt();
                        if (option1 == 1) {LogoutUI logoutUI = new LogoutUI();
                            logoutUI.run();}
                        if (option1 == 6) {SNSUserUI snsUserUI = new SNSUserUI();
                            snsUserUI.run();}
                        if (option1 == 7) {ArrivalUI arrivalUI = new ArrivalUI();
                            arrivalUI.run();}
                        if (option1 == 9) {ScheduleVaccineUI scheduleVaccineUI = new ScheduleVaccineUI();
                            scheduleVaccineUI.run();}
                        if (option1 == 0) {System.exit(1);}
                    }while (option1 != 1);
                    break;

                case ("Admin"):
                    int option = -1;
                    do {
                        System.out.println("1 - Logout");
                        System.out.println("2 - Know the Development Team");
                        System.out.println("3 - Register new Vaccination Center");
                        System.out.println("4 - Register new Employee");
                        System.out.println("5 - Register new VaccineType");
                        System.out.println("10 - Load a set of users from CSV file and register them");
                        System.out.println("0 - Exit");
                        System.out.println("Type your option:");
                        option = ler.nextInt();
                        if (option == 1) {LogoutUI logoutUI = new LogoutUI();
                            logoutUI.run();}
                        if (option == 2) {DevTeamUI devTeamUI = new DevTeamUI();
                            devTeamUI.run();}
                        if (option == 3) {AddCenterUI addCenterUI = new AddCenterUI();
                            addCenterUI.run();}
                        if (option == 4) {AddUserUI addUserUI = new AddUserUI();
                            addUserUI.run();}
                        if (option == 5) {AddVaccineTypeUI addVaccineTypeUI = new AddVaccineTypeUI();
                            addVaccineTypeUI.run();}
                        if (option == 10) {UsersCSVFileUI usersCSVFileUI = new UsersCSVFileUI();
                            usersCSVFileUI.run();}
                        if (option == 0) {System.exit(1);}
                    } while (option != 1);
                    break;

                case ("Nurse")  :
                    int option2 = -1;
                    do {
                        System.out.println("1 - Logout");
                        System.out.println("8 - Checks the Waiting Room");
                        System.out.println("12 - Record the administration of a vaccine");
                        System.out.println("0 - Exit");
                        System.out.println("Type your option:");
                        option2 = ler.nextInt();
                        if (option2 == 1) {LogoutUI logoutUI = new LogoutUI();
                            logoutUI.run();}
                        if (option2 == 8) {WaitingRoomUI waitingRoomUI = new WaitingRoomUI();
                            waitingRoomUI.run();}
                        if (option2 == 12) {AdministrateVaccineUI administrateVaccineUI = new AdministrateVaccineUI();
                            administrateVaccineUI.run();}
                        if (option2 == 0) {System.exit(1);}
                    } while (option2 != 1);
                    break;

                case ("Coordinator") :
                    int option3 = -1;
                    do {
                        System.out.println("1 - Logout");
                        System.out.println("11 - Analyze Performance of a Center");
                        System.out.println("0 - Exit");
                        System.out.println("Type your option:");
                        option3 = ler.nextInt();
                        if (option3 == 1) {LogoutUI logoutUI = new LogoutUI();
                            logoutUI.run();}
                        if (option3 == 11) {PerformanceUI performanceUI = new PerformanceUI();
                            performanceUI.run();}
                        if (option3 == 0) {System.exit(1);}
                    }while (option3 != 1);
                    //options.add(new MenuItem("Analyze Performance of a Center", new PerformanceUI()));
                    break;
            }

        }

        }
    }



