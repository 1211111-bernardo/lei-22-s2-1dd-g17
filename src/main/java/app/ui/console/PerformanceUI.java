package app.ui.console;

import Mappers.PMapper;
import app.controller.PerformanceController;
import app.domain.shared.PerformanceCenter;
import app.ui.console.utils.Utils;
import dto.PerformanceDTO;

import java.io.FileNotFoundException;
import java.sql.SQLOutput;
import java.text.ParseException;

public class PerformanceUI implements Runnable {
    private PerformanceController PCctrl;

    public PerformanceUI() {
        PCctrl = new PerformanceController();
    }

    public void run() {
        /**
         * In this section all necessary data to analyze a specific center will be requested
         */
        String center = Utils.readLineFromConsole("Enter the center you want to analyze:");
        String Date = Utils.readLineFromConsole("Enter the day you want to analyze:");
        String m = Utils.readLineFromConsole("Enter the time interval, in minutes:");
        PCctrl.createPerformanceCenter(center, Date, m);
        PerformanceCenter PC = new PerformanceCenter(Date, m);
        System.out.println(PC.ToString());
        String Confirmation = Utils.readLineFromConsole("Do you want to confirm?");
        /**
         * After typing the requested data all data will appear again a confirmation will be requested
         * If the answer is yes the data will be processed and the performance analyze continue
         * @catch FileNotFoundException if the data file is not founded
         * @catch ParseException if the read date if invalid
         */
        if (Confirmation.equals("yes")) {
            try {
                PCctrl.savePerformanceCenter(Date, m);
            } catch (FileNotFoundException | ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
