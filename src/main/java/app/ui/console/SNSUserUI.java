package app.ui.console;

import app.controller.SNSUserController;
import app.domain.Store.SNSUserSto;
import app.domain.model.SNSUser;
import app.ui.console.utils.Utils;

public class SNSUserUI implements Runnable{

    private SNSUserController SNcntrl;

    public SNSUserUI() {
        SNcntrl = new SNSUserController();
    }


    @Override
    public void run() {
        String role = "SNS User";
        System.out.printf("Register new SNS User:");
        String name = Utils.readLineFromConsole("Enter SNS User name: ");
        String email = Utils.readLineFromConsole("Enter SNS User email: ");
        String adress = Utils.readLineFromConsole("Enter SNS User address: ");
        String PhoneNumber = Utils.readLineFromConsole("Enter SNS User Phone Number: ");
        String CitizenCardNumber = Utils.readLineFromConsole("Enter Citizen Card Number: ");
        String SNSUserNumber = Utils.readLineFromConsole("Enter SNS User number: ");
        String BirthDate = Utils.readLineFromConsole("Enter BirthDate ");
        String Sex = Utils.readLineFromConsole("Enter Sex(optional): ");
        String pwd = SNcntrl.generatepwd(name, email, adress, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber);
        System.out.println("");
        SNcntrl.addUserWithRole(name, email, adress, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber);
        if (Sex.equals("")) {
            SNSUser user = new SNSUser(name, email, adress, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber);
        } else {
            SNSUser user = new SNSUser(name, adress, email, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber, Sex);
        }
        System.out.printf("Name: %s \n", name);
        System.out.printf(" Address: %s\n", adress);
        System.out.printf(" Card Number: %s\n", CitizenCardNumber);
        System.out.printf(" Phone Number: %s\n", PhoneNumber);
        System.out.printf(" SNS User number: %s\n", SNSUserNumber);
        System.out.printf("Password: %s \n", pwd);
        String confirmation = Utils.readLineFromConsole("Do you confirm the data? ");
        if (confirmation.equals("yes")) {
            if (Sex.equals("")) {
                SNSUserSto user1 = new SNSUserSto(name, adress, email, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber);
                if (user1.validateUser(email)) {
                    System.out.println("Operation failed");
                } else {
                    user1.addSNSUser(name, email, pwd, role);
                    user1.saveSNSUser(name, adress, email, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber);
                    System.out.println("SNS User added");
                }

            } else {
                SNSUserSto user2 = new SNSUserSto(name, adress, email, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber, Sex);
                if (user2.validateUser(email)) {
                    System.out.println("Operation failed");


                } else {
                    user2.addSNSUser(name, email, pwd, role);
                    user2.saveSNSUser(name, adress, email, PhoneNumber, BirthDate, SNSUserNumber, CitizenCardNumber, Sex);
                    System.out.println("SNS User added");
                }
            }
        } else{
            System.out.println("Try again!");
        }
    }
}

