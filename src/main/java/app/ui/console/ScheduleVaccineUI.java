package app.ui.console;


import app.controller.ScheduleVaccineController;
import app.domain.Store.ScheduleVaccineStore;
import app.domain.model.SNSUser;
import app.domain.shared.ArrivalUser;
import app.domain.shared.ScheduleVaccine;
import app.ui.console.utils.Utils;
import dto.ScheduleVaccineDTO;

import java.util.ArrayList;

public class ScheduleVaccineUI implements Runnable {
    private final ScheduleVaccineController SVcontroller;

    public ScheduleVaccineUI() {
        SVcontroller = new ScheduleVaccineController();
    }



    @Override
    public void run() {
        System.out.println("Schedule Vaccine:");
        ScheduleVaccineDTO dto;
        String SnsUserNumber = Utils.readLineFromConsole("Enter SNS User Number: ");
        ArrayList<SNSUser> SnsUserList = SVcontroller.getSnsUserList();
        ScheduleVaccineStore svstore = new ScheduleVaccineStore(SnsUserNumber);
        if (svstore.ValidateSnsUserNumber(SnsUserList, SnsUserNumber)==false){
            /**
             * In this condition the program validates the Sns USer number
             * The programme only stops if the SnsUserNumber doesnt exist in the system
             */
            SVcontroller.setSnsUserNumber(SnsUserNumber);
            String VaccinationCenter = Utils.readLineFromConsole("Enter the Vaccination Center Name: ");
            SVcontroller.setVaccinationCenter(VaccinationCenter);
            String Date = Utils.readLineFromConsole("Enter the Date you want to be vaccinated: ");
            SVcontroller.setDate(Date);
            String Time = Utils.readLineFromConsole("Enter the Time you want to be vaccinated: ");
            SVcontroller.setTime(Time);
            String Vaccine = Utils.readLineFromConsole("Enter the Vaccine you want to take: ");
            SVcontroller.setVaccine(Vaccine);
            dto = new ScheduleVaccineDTO(SnsUserNumber, VaccinationCenter, Date, Time, Vaccine);
            SVcontroller.createScheduleVaccine(dto);
            ScheduleVaccine ScheduleVaccine = new ScheduleVaccine(dto.getSnsUserNumber(), dto.getVaccinationCenter(), dto.getDate(), dto.getTime(), dto.getVaccine());
            if (svstore.checkDuplicatedVaccineApoointment(ScheduleVaccine)==false) {
                /**
                 * In this condition the program checks if the VaccineAppointment already exists
                 * The programme only stops if the VaccineAppointment already exists
                 */
                System.out.println(ScheduleVaccine.toString());
                String confirmation = Utils.readLineFromConsole("Do you confirm the data? ");
                if (confirmation.equals("yes")) {
                    if (svstore.validateScheduleVaccine(SnsUserNumber, VaccinationCenter, Date, Time, Vaccine)) {
                        System.out.println("Invalid Registration");
                    } else {
                        svstore.addScheduleVaccine(SnsUserNumber, VaccinationCenter, Date, Time, Vaccine);
                        System.out.println("Vaccine Appointment added");
                        String authorization = Utils.readLineFromConsole("Do you want to receive a SMS with information about the scheduled appointment? ");
                    }
                }
            } else {
                System.out.println("Your Vaccine Appointment already been scheduled");
            }
        } else {
            System.out.println("Sns User Number Invalid");
        }
    }
}