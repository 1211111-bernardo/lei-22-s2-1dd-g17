package app.ui.console;
/*
import app.controller.SpecifyNewVaccineController;
import app.domain.model.AdministrationProcess;
import app.domain.model.Vaccine;
import app.domain.shared.VaccineType;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



/**
 * @author : Daniel Coutinho - 1201440
 **/
/*public class SpecifyNewVaccineUI implements Runnable{

    SpecifyNewVaccineController vaccCtrl;

    public SpecifyNewVaccineUI(){
        vaccCtrl = new SpecifyNewVaccineController();
    }
    public void run() {

        System.out.println("\n\n" + ((specifyVaccine()) ? "Success on specifying new vaccine" : "Error on specifying new vaccine"));

    }

    public boolean specifyVaccine(){

        boolean success = false;

        do{
            String brand = Utils.readLineFromConsole("Vaccine brand: ");

            String vtTemp = Utils.readLineFromConsole("Vaccine type name: ");

            int nAg = Utils.readIntegerFromConsole("Number of different age groups: ");
            List<Integer> ag = new ArrayList<>();
            int cont = 0;
            System.out.println("Insert the lower limit and then the upper limit");
            for(int i=0;i<nAg*2;i++){
                System.out.print("\n"+(cont+1)+"º age of group ");
                if(i % 2 == 0) {
                    cont++;
                    System.out.println("[Lower limit]");
                }else
                    System.out.println("[Upper limit]");

                int agTemp = Utils.readIntegerFromConsole("");
                ag.add(agTemp);
            }

            System.out.println("\nDosage per age group [mL], respectively");
            int k=0;
            List<Float> d = new ArrayList<>();
            for(int i=0;i<nAg;i++){
                System.out.println("["+ag.get(k)+", "+ag.get(k+1)+"] age group");
                k+=2;
                float dTemp = (float)Utils.readDoubleFromConsole("");
                d.add(dTemp);
            }
            k=0;
            List<Integer> dn = new ArrayList<>();
            System.out.println("\nNumber of doses per age group, respectively");
            for(int i=0;i<nAg;i++){
                System.out.println("["+ag.get(k)+", "+ag.get(k+1)+"] age group");
                k+=2;
                int dnTemp = Utils.readIntegerFromConsole("");
                dn.add(dnTemp);
            }

            System.out.println("Time interval between doses (days)");
            List<Integer> ti = new ArrayList<>();
            k=0;
            for(int i=0;i<dn.size();i++){
                System.out.println("["+ag.get(k)+", "+ag.get(k+1)+"] age group");
                k+=2;
                for(int j=0;j<dn.get(i)-1;j++){
                    System.out.println((j+1) + "º dose to " + (j+2) + "º dose");
                    int tiTemp = Utils.readIntegerFromConsole("");
                    ti.add(tiTemp);
                }
            }

            try{
                VaccineType vacTy = new VaccineType();
                AdministrationProcess addPro = new AdministrationProcess(d, ag, dn, ti);
                vaccCtrl.createAdministrationProcess(addPro);

                vaccCtrl.createVaccine(brand, addPro, vacTy);
                success = true;

                vaccCtrl.getVaccines();
                if(Utils.confirm("Does data check? (s/n)"))
                    vaccCtrl.saveVaccineType();
                else{
                    System.out.println("Vaccine wasn't created!");
                    return false;
                }
            }catch(Exception e){
                System.out.println("An error occurred specifying the vaccine: " + e);
                return false;
            }
        }while(!success);

        return true;
    }
}*/
