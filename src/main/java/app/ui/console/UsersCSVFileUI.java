package app.ui.console;


import app.controller.UsersCSVFileController;

/*import java.io.File;*/
import java.io.FileNotFoundException;
import java.util.Scanner;

public class UsersCSVFileUI implements Runnable  {
    private UsersCSVFileController control;

    /**
     * Instantiates a new UsersCSVFileController.
     */
    public UsersCSVFileUI(){ control = new UsersCSVFileController();}

    /**
     * Method that runs all the UI code
     */
    public void run() {

        try {//TODO
            Scanner in = new Scanner(System.in);
            System.out.println("Insert file path");
            String path = in.nextLine();
            if(control.saveUsersData(path)){
                System.out.println("All data was inserted correctly");
            }else{
                System.out.println("The data wasnt saved due to the wrong information on the file");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}