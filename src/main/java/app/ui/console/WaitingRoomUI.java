package app.ui.console;


import app.controller.WaitingRoomController;
import app.domain.model.SNSUser;
import app.domain.shared.ArrivalUser;
import app.ui.console.utils.Utils;
import dto.AUserDto;
import dto.WaitingRoomDto;

import java.util.ArrayList;

public class WaitingRoomUI implements Runnable {
    private WaitingRoomController WaitingRoomCntrl;

    public WaitingRoomUI() {
        WaitingRoomCntrl = new WaitingRoomController();

    }

    int i=0;
    public void run(){
        i=i+1;
        WaitingRoomDto dto;

        /**
         * The programme only request the vaccination center once per run
         * All ArrivalUsers registers are associated with the respective center
         * If i is different than 1 the programme will not request the vaccination center avoiding repetition in terms of requested data
         *
         */

        if (i==1){
            String VaccinationCenter = Utils.readLineFromConsole("Enter the Vaccination Center Name: ");
            WaitingRoomCntrl.setVaccinationCenter(VaccinationCenter);
        }


        /**
         * Console gets the confirmation and validates it
         * If confirmation equals "yes", proceeds to get the 2 arraylists required to execute a following method
         * Uses WaitingRoomCntrl to have access to all the following methods
         * Method getSnsUserList- returns snsuserlist
         * Method getARRIVAL_USER_ARRAY_LIST- returns arrivaluserarraylist
         * Method createWaiting- gets by parameter the 2 arraylists
         * Method createWaitingList- creates a waitinglist
         * Method printarray- prints/shows the objects in the waitinglist with the necessary parameters
         */

        String confirmation = Utils.readLineFromConsole("Do you confirm the data? ");
        if (confirmation.equals("yes")) {
            ArrayList<SNSUser> snsUserArrayList = WaitingRoomCntrl.getSnsUserList();
            ArrayList<ArrivalUser> ArrivalList = WaitingRoomCntrl.getARRIVAL_USER_ARRAY_LIST();
            dto = new WaitingRoomDto(ArrivalList, snsUserArrayList);
            WaitingRoomCntrl.createWaiting(dto);
            WaitingRoomCntrl.createWaitingList(ArrivalList, snsUserArrayList);
                WaitingRoomCntrl.printarray(WaitingRoomCntrl.getWaitingList());

        }else{
            System.out.println("Please try again!");
        }

    }

}
