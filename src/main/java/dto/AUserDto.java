package dto;

import app.domain.shared.ArrivalUser;

public class    AUserDto {
    /**
     * All attributes needed to instantiate a new employee
     */
    private String SNSNumber;
    private String Time;
    /**
     * Creates an instance of ArrivalUser.
     * @param SNSNumber
     * @param Time

     */
    public AUserDto (String SNSNumber,String Time) {
        this.SNSNumber = SNSNumber;
        this.Time = Time;
    }
    /**
     * Shows the Number of the SNS User.
     *
     * @return the name of the implicit SNS User.
     */
    public String getSNSNumber() {
        return SNSNumber;
    }
    /**
     * Shows the Time that the SNS User arrived.
     *
     * @return the time that the SNS User arrived.
     */
    public String getTime() {
        return Time;
    }
    /**
     * Sets the SNSNumber of the implicit ArrivalUSer
     *
     * @param SNSNumber changes the old number to this, passed by argument.
     */

    public void setSNSNumber(String SNSNumber) {
        this.SNSNumber = SNSNumber;
    }

}

