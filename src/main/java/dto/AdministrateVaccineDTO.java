package dto;

import app.domain.Store.SNSUserSto;
import app.domain.model.SNSUser;
import app.domain.shared.AdministrateVaccine;

import java.util.ArrayList;

public class AdministrateVaccineDTO {

    /**
     * The SNS User List that contains all user information
     */
    private ArrayList<SNSUser> snsUserList= new ArrayList<>();


    /**
     * All attributes needed to instantiate a new vaccine administration
     */
    private String snsUserNumber;
    private String vaccine;
    private String dose;
    private String type;


    /**
     * Creates an instance of AdministrateVaccine.
     * @param snsUserNumber
     * @param vaccine
     * @param dose
     * @param type

     */
    public AdministrateVaccineDTO(String snsUserNumber, String type, String vaccine, String dose) {
        this.snsUserNumber = snsUserNumber;
        this.type = type;
        this.vaccine = vaccine;
        this.dose = dose;
    }

    /**
     * Shows the SnsUserList.
     *
     * @return the list of SNS Users.
     */
    public ArrayList<SNSUser> getSnsUserList() {
        SNSUserSto sto= new SNSUserSto();
        return snsUserList;
    }

    /**
     * Shows the Number of the SNS User.
     *
     * @return the name of the implicit SNS User.
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    /**
     * Shows the Vaccine.
     *
     * @return the vaccine.
     */
    public String getVaccine() {
        return vaccine;
    }

    /**
     * Shows the Dose.
     *
     * @return the dose.
     */
    public String getDose() {
        return dose;
    }


    /**
     * Shows the Type.
     *
     * @return the type.
     */
    public String getType() {
        return type;
    }

    /**
     * This method is needed in the UI to create a new AdministrateVaccine Object
     */
    public AdministrateVaccineDTO(){

    }
    /**
     * This method is responsible for adding a new Vaccine Administration
     * Gets by parameter all data required to register a new Vaccine Administration
     * @param SnsUserNumber
     * @param Vaccine
     * @param Dose
     * @param Type
     */
    public void addAdministrateVaccine(String SnsUserNumber, String Type, String Vaccine, String Dose) {
        AdministrateVaccine av = new AdministrateVaccine(SnsUserNumber, Type, Vaccine, Dose);
    }
}
