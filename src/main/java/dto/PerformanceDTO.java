package dto;

public class PerformanceDTO {
    private String Date;
    private String TimeInterval;

    /**
     * Constructor that allows to instantiate a new PerformanceDTO
     * @param Date corresponds to the date that the center will be analyzed
     * @param TimeInterval corresponds to the time interval of the analysis
     */
    public PerformanceDTO (String Date, String TimeInterval) {
        this.Date = Date;
        this.TimeInterval = TimeInterval;
    }

    /**
     * @return the corresponding date
     */
    public String getDate() {
        return Date;
    }

    /**
     * @return the corresponding TimeInterval
     */
    public String getTimeInterval() {
        return TimeInterval;
    }

}
