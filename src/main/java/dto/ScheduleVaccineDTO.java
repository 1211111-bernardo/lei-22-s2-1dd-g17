package dto;

import app.domain.Store.ArrivalStore;
import app.domain.Store.SNSUserSto;
import app.domain.model.SNSUser;
import app.domain.shared.ArrivalUser;
import app.domain.shared.ScheduleVaccine;

import java.util.ArrayList;

public class ScheduleVaccineDTO {

    private ArrayList<SNSUser> SnsUserList = new ArrayList<>();

    private String SnsUserNumber;
    private String Time;
    private String VaccinationCenter;
    private String Date;
    private String Vaccine;

    public ScheduleVaccineDTO (String SnsUserNumber, String VaccinationCenter , String Date, String Time, String Vaccine) {
        this.SnsUserNumber = SnsUserNumber;
        this.Time = Time;
        this.VaccinationCenter = VaccinationCenter;
        this.Date = Date;
        this.Vaccine = Vaccine;
    }

    public ScheduleVaccineDTO() {

    }

    public ArrayList<SNSUser> getSnsUserList() {
        SNSUserSto sto= new SNSUserSto();
        return SnsUserList;
    }


    public String getSnsUserNumber() {
        return SnsUserNumber;
    }

    public String getVaccinationCenter(){return VaccinationCenter;}


    public String getDate(){return Date;}

    public String getVaccine(){return Vaccine;}

    public String getTime(){return Time;}


    public void setSnsUserNumber(String SnsUsernumber) {
        this.SnsUserNumber = SnsUsernumber;
    }

}



