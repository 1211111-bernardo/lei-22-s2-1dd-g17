package dto;

import app.domain.Store.ArrivalStore;
import app.domain.Store.SNSUserSto;
import app.domain.model.SNSUser;
import app.domain.shared.ArrivalUser;

import java.util.ArrayList;

public class WaitingRoomDto {

    private ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST = new ArrayList<>();

    private ArrayList<SNSUser> snsUserList= new ArrayList<>();

    public WaitingRoomDto(ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST, ArrayList<SNSUser> snsUserList) {
        this.ARRIVAL_USER_ARRAY_LIST = this.ARRIVAL_USER_ARRAY_LIST;
        this.snsUserList = this.snsUserList;
    }

    public WaitingRoomDto(){

    }

    public ArrayList<ArrivalUser> getARRIVAL_USER_ARRAY_LIST() {
        ArrivalStore sto= new ArrivalStore();
        return sto.getArrivalUserArrayList();
    }

    public void setARRIVAL_USER_ARRAY_LIST(ArrayList<ArrivalUser> ARRIVAL_USER_ARRAY_LIST) {
        this.ARRIVAL_USER_ARRAY_LIST = ARRIVAL_USER_ARRAY_LIST;
    }

    public ArrayList<SNSUser> getSnsUserList() {
        SNSUserSto sto= new SNSUserSto();
        return sto.getSnsUserList();
    }

    public void setSnsUserList(ArrayList<SNSUser> snsUserList) {
        this.snsUserList = snsUserList;
    }
}
